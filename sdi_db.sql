-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Agu 2021 pada 07.33
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdi_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absent`
--

CREATE TABLE `absent` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `status` enum('Masuk','Tidak Masuk') NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `date_absent` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `absent`
--

INSERT INTO `absent` (`id`, `student_id`, `status`, `kelas_id`, `date_absent`) VALUES
(1, 1, 'Masuk', 1, '2021-08-28'),
(2, 1, 'Masuk', 1, '2021-08-29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id` int(20) NOT NULL,
  `teacher_id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id`, `teacher_id`, `name`) VALUES
(1, 1, 'Kelas 1'),
(2, 2, 'Kelas 2'),
(3, 3, 'Kelas 3'),
(4, 4, 'Kelas 4'),
(5, 5, 'Kelas 5'),
(6, 6, 'Kelas 6');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matpel`
--

CREATE TABLE `matpel` (
  `id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `point_kkm` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `matpel`
--

INSERT INTO `matpel` (`id`, `kelas_id`, `name`, `point_kkm`) VALUES
(1, 1, 'Matematika', 75),
(2, 2, 'Bahasa Inggris', 75),
(3, 1, 'Bahasa Indonesia', 85),
(4, 1, 'IPA', 70),
(5, 1, 'IPS', 70);

-- --------------------------------------------------------

--
-- Struktur dari tabel `point`
--

CREATE TABLE `point` (
  `id` int(11) NOT NULL,
  `matpel_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `point` varchar(50) NOT NULL,
  `date_point` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `point`
--

INSERT INTO `point` (`id`, `matpel_id`, `student_id`, `kelas_id`, `point`, `date_point`) VALUES
(1, 1, 1, 1, '90', '2021-08-28'),
(2, 1, 1, 1, '70', '2021-08-29'),
(3, 3, 1, 1, '95', '2021-08-29'),
(4, 4, 1, 1, '75', '2021-08-29'),
(5, 5, 1, 1, '80', '2021-08-29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `nis` int(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `student`
--

INSERT INTO `student` (`id`, `nis`, `student_id`, `users_id`, `kelas_id`) VALUES
(1, 202101, 1, 2, 1),
(2, 2021102, 2, 6, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `student_register`
--

CREATE TABLE `student_register` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `nama_p` varchar(10) NOT NULL,
  `umur` varchar(10) NOT NULL,
  `jen_kel` enum('Laki-laki','Perempuan') NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `nama_a` varchar(50) NOT NULL,
  `nama_i` varchar(50) NOT NULL,
  `pekerjaan_a` varchar(20) NOT NULL,
  `pekerjaan_i` varchar(20) NOT NULL,
  `penghasilan` varchar(20) NOT NULL,
  `status` enum('diproses','diterima') NOT NULL,
  `bukti_tf` varchar(50) DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `student_register`
--

INSERT INTO `student_register` (`id`, `users_id`, `nama_lengkap`, `nama_p`, `umur`, `jen_kel`, `tempat_lahir`, `tgl_lahir`, `nama_a`, `nama_i`, `pekerjaan_a`, `pekerjaan_i`, `penghasilan`, `status`, `bukti_tf`, `created_at`) VALUES
(1, 2, 'Jono', 'Suteno', '7', 'Laki-laki', 'Jakarta', '2011-08-12', 'Jono', 'Jini', 'Developer', 'QC', '>1.000.000', 'diterima', 'E9Zlf2nVcAIhMEf.jpg', '2021-08-28'),
(2, 6, 'Annisa Yunarti', 'Anisa', '9', 'Perempuan', 'Jakarta', '2007-12-31', 'Jono', 'Jini Putri', 'PNS', 'PNS', '>1.000.000', 'diterima', NULL, '2021-08-29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `code_teacher` varchar(50) NOT NULL,
  `name` varchar(30) NOT NULL,
  `place_ofbirth` varchar(50) NOT NULL,
  `date_birth` date NOT NULL,
  `education` varchar(50) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `teachers`
--

INSERT INTO `teachers` (`id`, `users_id`, `code_teacher`, `name`, `place_ofbirth`, `date_birth`, `education`, `created_at`) VALUES
(1, 3, 'SDI0001', 'guru', 'Jakarta', '1992-02-04', 'SMA', '2021-08-28'),
(2, 4, 'SDI0002', 'guru2', 'Jakarta', '1992-01-01', 'SMA', '2021-08-28'),
(3, 7, 'SDI0003', 'guru3', 'Jakarta', '1992-02-04', 'SMA', '2021-08-28'),
(4, 8, 'SDI0004', 'guru4', 'Jakarta', '1992-01-01', 'SMA', '2021-08-28'),
(5, 9, 'SDI0005', 'guru5', 'Jakarta', '1992-02-04', 'SMA', '2021-08-28'),
(6, 10, 'SDI0006', 'guru6', 'Jakarta', '1992-01-01', 'SMA', '2021-08-28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `status` enum('admin','teacher','wali') NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `image`, `phone`, `address`, `status`, `created_at`) VALUES
(1, 'admin', '$2y$10$2dkomm92GSskX8jzukYYUu1OZicKRRzUIBFIJETBZ/RMyDNlJVCEK', 'admin', 'undraw_profile.svg', '0897878787878', 'Bekasi, Jatiwaringin', 'admin', '2021-08-28'),
(2, 'wali', '$2y$10$OzGaA.kFmgX.RbLaf5kqi.38ctTJNkCLQhXyN2d6P7IqbKmHR3P2K', 'user lengkap ', 'undraw_profile.svg', '09008989', 'Depok', 'wali', '2021-08-28'),
(3, 'guru', '$2y$10$Hkp/DcJEOLnTp7mv.sF0guGWkW4ExZKjIH1UUqTQaqLfY53dVLYb2', 'guru', 'isaac-smith-6EnTPvPPL6I-unsplash.jpg', '8989898989', 'depok', 'teacher', '2021-08-28'),
(4, 'guru2', '$2y$10$WwJAkfds0NUEvllBfkFs0ePx4R2D.jRViliAENGw0Ah7Kh3icIopW', 'guru2', 'david-travis-WC6MJ0kRzGw-unsplash.jpg', '090908080', 'depok', 'teacher', '2021-08-28'),
(6, 'wali2', '$2y$10$A30Rti.9OKmXp78rSrXba.gd7X.8TO7/mJyU6Wnl211biHbO2jnga', 'wali2', 'undraw_profile.svg', '089189189189', 'depok', 'wali', '2021-08-28'),
(7, 'guru3', '$2y$10$Hkp/DcJEOLnTp7mv.sF0guGWkW4ExZKjIH1UUqTQaqLfY53dVLYb2', 'guru3', 'isaac-smith-6EnTPvPPL6I-unsplash.jpg', '8989898989', 'depok', 'teacher', '2021-08-28'),
(8, 'guru4', '$2y$10$WwJAkfds0NUEvllBfkFs0ePx4R2D.jRViliAENGw0Ah7Kh3icIopW', 'guru4', 'david-travis-WC6MJ0kRzGw-unsplash.jpg', '090908080', 'depok', 'teacher', '2021-08-28'),
(9, 'guru5', '$2y$10$Hkp/DcJEOLnTp7mv.sF0guGWkW4ExZKjIH1UUqTQaqLfY53dVLYb2', 'guru5', 'isaac-smith-6EnTPvPPL6I-unsplash.jpg', '8989898989', 'depok', 'teacher', '2021-08-28'),
(10, 'guru6', '$2y$10$WwJAkfds0NUEvllBfkFs0ePx4R2D.jRViliAENGw0Ah7Kh3icIopW', 'guru6', 'david-travis-WC6MJ0kRzGw-unsplash.jpg', '090908080', 'depok', 'teacher', '2021-08-28');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `absent`
--
ALTER TABLE `absent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`kelas_id`),
  ADD KEY `absent_ibfk_1` (`student_id`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `matpel`
--
ALTER TABLE `matpel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`kelas_id`);

--
-- Indeks untuk tabel `point`
--
ALTER TABLE `point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matpel_id` (`matpel_id`,`student_id`,`kelas_id`),
  ADD KEY `teacher_id` (`kelas_id`),
  ADD KEY `point_ibfk_2` (`student_id`);

--
-- Indeks untuk tabel `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `teacher_id` (`kelas_id`);

--
-- Indeks untuk tabel `student_register`
--
ALTER TABLE `student_register`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indeks untuk tabel `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `absent`
--
ALTER TABLE `absent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `matpel`
--
ALTER TABLE `matpel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `point`
--
ALTER TABLE `point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `student_register`
--
ALTER TABLE `student_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
