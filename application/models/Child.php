<?php

class Child extends CI_Model
{

	public function selectAll($user_id)
	{
		$query = $this->db->query("
		SELECT student.student_id,student_register.nama_lengkap,
		student.kelas_id, teachers.name as nama_guru, kelas.name as nama_kelas
		FROM student_register
		LEFT JOIN student ON student_register.id = student.student_id
		LEFT JOIN kelas ON student.kelas_id = kelas.id
		LEFT JOIN teachers ON kelas.teacher_id = teachers.id
		WHERE student_register.users_id = '{$user_id}';
		");
		return $query;
	}

	public function showAbsent($stud_id, $teach_id, $today)
	{
		$query = $this->db->query("
		SELECT absent.*, kelas.name
		FROM absent 
		LEFT JOIN kelas ON absent.kelas_id = kelas.id
		WHERE absent.student_id = '{$stud_id}' and absent.kelas_id = '{$teach_id}' and absent.date_absent = '{$today}'
		")->result();
		return $query;
	}


	public function showPoint($stud_id, $teach_id, $today)
	{
		$query = $this->db->query("
		SELECT point.id, point.point, matpel.point_kkm, point.date_point
		,matpel.id AS matpel_id, matpel.name, matpel.point_kkm, student_register.nama_lengkap,
		student_register.id as student_id
		FROM point
		LEFT JOIN matpel ON point.matpel_id = matpel.id
		LEFT JOIN student_register on point.student_id = student_register.id
		WHERE point.student_id = '{$stud_id}' AND point.kelas_id = '{$teach_id}' and point.date_point = '{$today}'
		")->result();
		return $query;
	}	



}

?>
