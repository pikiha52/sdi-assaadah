<?php

class Users extends CI_Model
{

	public function getLastId()
	{
		$query = "SELECT id FROM users ORDER BY id DESC LIMIT 1";
		return $this->db->query($query)->result_array();
	}

	public function selectAll()
	{
		$query = $this->db->query("
		SELECT * FROM users WHERE NOT status = 'admin'
		")->result();
		return $query;
	}

	public function check($id)
	{
		$query = $this->db->query("
		SELECT * FROM users WHERE id = '{$id}'
		");
		return $query;
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE users, teachers FROM users INNER JOIN teachers
			WHERE users.id = teachers.users_id AND users.id = {$id}
			");
		return $query;
	}

	public function destroyWali($id)
	{
		$query = $this->db->query("
			DELETE users
			FROM users
			WHERE users.id = {$id}
			");
		return $query;
	}


}
?>
