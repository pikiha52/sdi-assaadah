<?php

class Kelas extends CI_Model
{

    public function getall()
	{
		$query = $this->db->query("
			SELECT * FROM kelas
		")->result();
		return $query;
	}

    public function getId($id)
	{
		$query = $this->db->query("
        SELECT kelas.*, teachers.*
        FROM kelas
        LEFT JOIN teachers ON kelas.teacher_id = teachers.id
        WHERE teachers.users_id = '{$id}'
		");
		return $query;
	}

}