<?php

class Subjects extends CI_Model
{

	public function selectAll()
	{
		$query = $this->db->query("
		SELECT matpel.*, kelas.name as nama_kelas, 
		teachers.name as nama_guru
		FROM matpel
		LEFT JOIN kelas on matpel.kelas_id = kelas.id
		LEFT JOIN teachers ON kelas.teacher_id = teachers.id;
		")->result();
		return $query;
	}

	public function countSubj()
	{
		$query = $this->db->query("
		SELECT matpel.*, teachers.name as name_guru
		FROM matpel
		LEFT JOIN kelas ON matpel.kelas_id = kelas.id
		LEFT JOIN teachers ON kelas.teacher_id = teachers.id
		GROUP BY matpel.name;
		")->result();
		return $query;
	}

	public function byId($id)
	{
		$query = $this->db->query("
		SELECT matpel.*, teachers.name as name_guru
		FROM matpel
		LEFT JOIN kelas ON matpel.kelas_id = kelas.id
		LEFT JOIN teachers ON kelas.teacher_id = teachers.id
		WHERE matpel.kelas_id = '{$id}';
		")->result();
		return $query;
	}

	public function store($tabel, $data)
	{
		return $this->db->insert($tabel, $data);
	}


}
?>
