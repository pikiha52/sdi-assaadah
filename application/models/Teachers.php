<?php

class Teachers extends CI_Model
{

	public function SelectAll()
	{
		$query = $this->db->query("
		SELECT teachers.*, users.image, kelas.name as nama_kelas
		FROM teachers
		LEFT JOIN users ON teachers.users_id = users.id
		LEFT JOIN kelas ON kelas.teacher_id = teachers.id;
		")->result();
		return $query;

	}

	public function getId($id)
	{
		$query = $this->db->query("
			SELECT * FROM teachers WHERE users_id = '{$id}'
		");
		return $query;
	}

	function getCode()
	{
		$this->db->select('RIGHT(code_teacher,3) as code', FALSE);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1); 
		$query = $this->db->get('teachers');
		if ($query->num_rows() <> 0) {
			$data = $query->row();
			$kode = intval($data->code) + 1;
		} else {
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = 'SDI' . $kodemax;
		return $kodejadi;
	}


}
?>
