<?php

class Students extends CI_Model
{

	public function selectRegister($id)
	{
		$query = $this->db->query("
		SELECT users.id, users.name, student_register.*
		FROM users 
		LEFT JOIN student_register ON users.id = student_register.users_id
		WHERE users.id = '{$id}'
		");
		return $query;
	}

	public function updateStatus($id)
	{
		$query = $this->db->query("
	    	SELECT * FROM `student_register` WHERE id = '{$id}' 
		");
		return $query;
	}

	public function selectregisAdmin()
	{
		$query = $this->db->query("
	    	SELECT * FROM `student_register` WHERE status = 'diproses'  
		")->result();
		return $query;
	}

	public function studActive($id)
	{
		$query = $this->db->query("
		SELECT student.*, users.name, 
		teachers.code_teacher, teachers.name as nama_guru, 
		student_register.nama_lengkap
		FROM student
		LEFT JOIN users ON student.users_id = users.id
		LEFT JOIN kelas ON student.kelas_id = kelas.id
		LEFT JOIN teachers ON kelas.teacher_id = teachers.id
		LEFT JOIN student_register ON student.student_id = student_register.id
		WHERE student.kelas_id = '{$id}';
		")->result();
		return $query;
	}

	public function studAct()
	{
		$query = $this->db->query("
		SELECT student.*, student_register.nama_lengkap, 
		kelas.name as nama_kelas, teachers.name as nama_guru
		FROM student
		LEFT JOIN student_register on student.student_id = student_register.id
		LEFT JOIN kelas on student.kelas_id = kelas.id
		LEFT JOIN teachers on kelas.teacher_id = teachers.id;
		")->result();
		return $query;
	}

	public function store($tabel, $data)
	{
		return $this->db->insert($tabel, $data);
	}

	public function byUpdateStatus($id)
	{
		$query = $this->db->query("
		SELECT student.*, users.name, 
		teachers.code_teacher, teachers.name as nama_guru, 
		student_register.*
		FROM student
		LEFT JOIN users ON student.users_id = users.id
		LEFT JOIN kelas ON student.kelas_id = kelas.id
		LEFT JOIN teachers ON kelas.teacher_id = teachers.id
		LEFT JOIN student_register ON student.student_id = student_register.id
		WHERE student.student_id = '{$id}';
		")->result();
		return $query;
	}

	public function showStud($stud_id, $teach_id, $today)
	{
		$query = $this->db->query("
		SELECT point.id, point.point, matpel.point_kkm, point.date_point
		,matpel.id AS matpel_id, matpel.name, matpel.point_kkm, student_register.nama_lengkap,
		student_register.id as student_id, kelas.name as nama_kelas
		FROM point
		LEFT JOIN matpel ON point.matpel_id = matpel.id
		LEFT JOIN student_register on point.student_id = student_register.id
		left join kelas on point.kelas_id = kelas.id
		WHERE point.student_id = '{$stud_id}' AND point.kelas_id = '{$teach_id}' and point.date_point = '{$today}'
	")->result();
	return $query;
	}

	public function showStudDate($stud_id, $teach_id, $date)
	{
		$query = $this->db->query("
		SELECT point.id, point.point, matpel.point_kkm, point.date_point
		,matpel.id AS matpel_id, matpel.name, matpel.point_kkm, student_register.nama_lengkap,
		student_register.id as student_id
		FROM point
		LEFT JOIN matpel ON point.matpel_id = matpel.id
		LEFT JOIN student_register on point.student_id = student_register.id
		WHERE point.student_id = '{$stud_id}' AND point.kelas_id = '{$teach_id}' and point.date_point = '{$date}'
	")->result();
	return $query;
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE point
			FROM point
			WHERE point.id = {$id}
			");
		return $query;
	}

	function getnis()
	{
		$this->db->select('RIGHT(nis,3) as code', FALSE);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1); 
		$query = $this->db->get('student');
		if ($query->num_rows() <> 0) {
			$data = $query->row();
			$kode = intval($data->code) + 1;
		} else {
			$kode = 1;
		}
		$today = date('Y');
		$kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT);
		$kodejadi = $today . $kodemax;
		return $kodejadi;
	}


}
?>
