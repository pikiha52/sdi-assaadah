<?php

class Absent extends CI_Model
{

	public function checkId($id, $today)
	{
		$query = $this->db->query("
		SELECT * FROM `absent` WHERE student_id = '{$id}' AND date_absent = '{$today}'
		")->result();
		return $query;
	}

	public function byId($stud_id, $teach_id, $today)
	{
		$query = $this->db->query("
		SELECT absent.*, kelas.name
		FROM absent 
		LEFT JOIN kelas ON absent.kelas_id = kelas.id
		WHERE absent.student_id = '{$stud_id}' and absent.kelas_id = '{$teach_id}' and absent.date_absent = '{$today}'
		")->result();
		return $query;
	}

	public function byDate($stud_id, $teach_id, $date)
	{
		$query = $this->db->query("
		SELECT absent.*, kelas.name
		FROM absent 
		LEFT JOIN kelas ON absent.kelas_id = kelas.id
		WHERE absent.student_id = '{$stud_id}' and absent.kelas_id = '{$teach_id}' and absent.date_absent = '{$date}'
		")->result();
		return $query;
	}

}
?>
