<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('students');
		$this->load->model('teachers');
	}


	public function index()
	{
		$data = [
			"title" => "Selamat Datang di Website Resmi SDI ASSA'ADDAH"
		];

		$this->load->view('template-frontend/header-sdi', $data);
		$this->load->view('frontend/sdi/home');
		$this->load->view('template-frontend/footer-sdi');
	}

	public function ppdb()
	{
		$data = [
			"title" => "PETUNJUK PPDB ONLINE"
		];

		$this->load->view('template-frontend/header-sdi', $data);
		$this->load->view('frontend/ppdb/petunjuk');
		$this->load->view('template-frontend/footer-sdi');
	}

	public function register()
	{
		$data = [
			"title" => "FORM REGISTRASI AKUN SDI ASSA'ADAH"
		];

		$this->load->view('template-frontend/header', $data);
		$this->load->view('frontend/landing-page/register');
		$this->load->view('template-frontend/footer');
	}

	public function siswa()
	{
		$siswa = $this->students->studAct();
		$data = [
			'title' => "SDI ASSA'ADAH - DATA SISWA",
			'siswa' => $siswa
		];

		$this->load->view('template-frontend/header-sdi', $data);
		$this->load->view('frontend/siswa/index');
		$this->load->view('template-frontend/footer-sdi');
	}

	public function guru()
	{
		$guru = $this->teachers->selectAll();
		$data = [
			'title' => "SDI ASSA'ADAH - DATA GURU",
			'guru' => $guru
		];

		$this->load->view('template-frontend/header-sdi', $data);
		$this->load->view('frontend/guru/index');
		$this->load->view('template-frontend/footer-sdi');
	}

	public function profile()
	{
		$data = [
			"title" => "SDI ASSA'ADAH - Profile Sekolah"
		];

		$this->load->view('template-frontend/header-sdi', $data);
		$this->load->view('frontend/profile-sekolah/index');
		$this->load->view('template-frontend/footer-sdi');
	}
	
	public function silabus()
	{
		$data = [
			"title" => "SDI ASSA'ADAH - Silabus"
		];
	
		$this->load->view('template-frontend/header-sdi', $data);
		$this->load->view('frontend/silabus');
		$this->load->view('template-frontend/footer-sdi');
	}
}
