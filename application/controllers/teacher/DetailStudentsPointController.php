<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailStudentsPointController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('teachers');
		$this->load->model('subjects');
		$this->load->model('students');
		$this->load->model('absent');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}


	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$teachers = $this->teachers->getId($id)->row();
		$teach_id = $teachers->id;
		$students = $this->students->studActive($teach_id);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/point');
		$this->load->view('template/footer');
	}

	public function showPoint($stud_id)
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$getId = $this->teachers->getId($id)->row();
		$teach_id = $getId->id;
		$subject = $this->subjects->byId($teach_id);
		$today = date('Y-m-d');
		$studName = $this->students->updateStatus($stud_id)->row();
		$students = $this->students->showStud($stud_id, $teach_id, $today);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today,
			'studName' => $studName, 
			'subject' => $subject,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/detail-point');
		$this->load->view('template/footer');
	}

	public function byDate($stud_id)
	{
		$date = $this->input->post('date');
		// jika tanggal kosong akan redirect ke function showpoint
		if ($date == NULL) {
			$this->session->set_flashdata('error', 'message_error');
			redirect(base_url('teacher/DetailStudentsPointController/showPoint/'. $stud_id));
		} else {
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$getId = $this->teachers->getId($id)->row();
		$teach_id = $getId->id;
		$students = $this->students->showStud($stud_id, $teach_id, $date);
		$today = date('Y-m-d');
		$studName = $this->students->updateStatus($stud_id)->row();
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today,
			'studName' => $studName,
			'name'	=> $name
		];


		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/detail-pointbydate');
		$this->load->view('template/footer');

		}
	}


	public function update()
	{
		$stud_id = $this->input->post('student_id');
		$date = date('Y-m-d');
		$point_id = $this->input->post('point_id');
		$matpel = $this->input->post('matpel');
		$point = $this->input->post('point');

		$data = array(

			'matpel_id' => $matpel,
			'point' => $point,
			'date_point' => $date

		);

		$query = $this->db->update('point', $data, array('id' => $point_id));;
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('teacher/DetailStudentsPointController/showPoint/'. $stud_id)); 

	}


	public function delete($id)
	{
	  $this->students->destroy($id);
	  $this->session->set_flashdata('success', 'message_success');
	  redirect(base_url('teacher/DetailStudentsPointController'));
	}



}
