<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('teachers');
		$this->load->model('users');
		$this->load->model('students');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$teachers = $this->teachers->getId($id)->row();
		$teach_id = $teachers->id;
		$students = $this->students->studActive($teach_id);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/dashboard/index');
		$this->load->view('template/footer');

	}



}
