<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailStudentsAbsentController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('teachers');
		$this->load->model('subjects');
		$this->load->model('students');
		$this->load->model('absent');
		$this->load->model('kelas');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}


	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$teachers = $this->teachers->getId($id)->row();
		$teach_id = $teachers->id;
		$students = $this->students->studActive($teach_id);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/absen');
		$this->load->view('template/footer');
	}

	public function showAbsent($stud_id)
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$getId = $this->kelas->getId($id)->row();
		$kelas_id = $getId->id;
		$today = date('Y-m-d');
		$students = $this->absent->byId($stud_id, $kelas_id, $today);
		$studName = $this->students->updateStatus($stud_id)->row();
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today,
			'studName' => $studName,
			'name' => $name
		];


		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/detail-absent');
		$this->load->view('template/footer');
	}


	public function byDate($stud_id)
	{
		$date = $this->input->post('date');
		if ($date == NULL) {
			$this->session->set_flashdata('error', 'message_error');
			redirect(base_url('teacher/DetailStudentsPointController/showAbsent/'. $stud_id));
		} else {
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$getId = $this->teachers->getId($id)->row();
		$teach_id = $getId->id;
		$students = $this->absent->byDate($stud_id, $teach_id, $date);
		$today = date('Y-m-d');
		$studName = $this->students->updateStatus($stud_id)->row();
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today,
			'studName' => $studName,
			'name'	=> $name
		];


		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/detail-absentbydate');
		$this->load->view('template/footer');

		}
	}


	public function update()
	{
		
		$stud_id = $this->input->post('student_id');
		$absen_id = $this->input->post('absent_id');
		$absent = $this->input->post('absent');

		$data = array(

			'id' => $absen_id,
			'status' => $absent,

		);

		$query = $this->db->update('absent', $data, array('id' => $absen_id));;
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('teacher/DetailStudentsAbsentController/showAbsent/'. $stud_id)); 

	}


	public function delete($id)
	{
	  $this->students->destroy($id);
	  $this->session->set_flashdata('success', 'message_success');
	  redirect(base_url('teacher/DetailStudentsAbsentController'));
	}



}
