<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentsController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('teachers');
		$this->load->model('subjects');
		$this->load->model('students');
		$this->load->model('absent');
		$this->load->model('kelas');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function detailStudent($stud_id)
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$getId = $this->teachers->getId($id)->row();
		$teach_id = $getId->id;
		$today = date('Y-m-d');
		$students = $this->students->showStud($stud_id, $teach_id);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/student/detail');
		$this->load->view('template/footer');
	}

	public function addAbsent()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$kelas = $this->kelas->getId($id)->row();
		$teach_id = $kelas->id;
		$today = date('Y-m-d');
		$students = $this->students->studActive($teach_id);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today, 
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/store/absen');
		$this->load->view('template/footer');

	}


	public function storeAbsent()
	{
		$id = $this->session->userdata('id');
		$kelas = $this->kelas->getId($id)->row();
		$kelas_id = $kelas->id;;
		$student_id = $this->input->post('student_id');
		$status = $this->input->post('status');
		$date = $this->input->post('date_absent');

		if($status == NULL){

			$this->session->set_flashdata('error', 'message_failed');
			redirect('teacher/StudentsController/addAbsent');
		
		} else {
			$data = array();
			foreach($student_id as $key => $val)
			{
				$data[] = array(
					'kelas_id' => $kelas_id,
					'student_id' => $val,
					'status' => $status[$key],
					'date_absent' => $date[$key],
				);

			}
	
			$insert = $this->db->insert_batch('absent', $data);
			if($insert){
				$this->session->set_flashdata('success', 'message_success');
				redirect('teacher/DashboardController');
			}
		}
	}

	public function addPoint()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$kelas = $this->kelas->getId($id)->row();
		$teach_id = $kelas->id;
		$today = date('Y-m-d');
		$subject = $this->subjects->byId($teach_id);
		$students = $this->students->studActive($teach_id);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'students' => $students,
			'today' => $today,
			'subject' => $subject,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-teacher/store/point');
		$this->load->view('template/footer');
	}


	public function storePoint()
	{
		$id = $this->session->userdata('id');
		$kelas = $this->kelas->getId($id)->row();
		$teacher_id = $kelas->id;;
		$student_id = $this->input->post('student_id');
		$matpel_id = $this->input->post('matpel');
		$point = $this->input->post('point');
		$date = $this->input->post('date_point');
		
		if($matpel_id == 0){

			$this->session->set_flashdata('error', 'message_failed');
			redirect('teacher/StudentsControllers/addPoint');
		
		} else {
			$data = array();
			foreach($student_id as $key => $val)
			{
				$data[] = array(
					'kelas_id' => $teacher_id,
					'student_id' => $val,
					'matpel_id' => $matpel_id[$key],
					'point' => $point[$key],
					'date_point' => $date[$key],

				);

			}

			$insert = $this->db->insert_batch('point', $data);
			if($insert){
				$this->session->set_flashdata('success', 'message_success');
				redirect('teacher/DashboardController');
			}

		}
	}


}
