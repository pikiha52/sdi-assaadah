<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
    }

	function silabus1()
	{
        $file_name = 'Silabus Kelas 1.zip';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);

	}
 
	function silabus2()
	{
        $file_name = 'Silabus Kelas 2.zip';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);
	}
 
	function silabus3()
	{
        $file_name = 'Silabus Kelas 3.zip';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);
	}
 
	function silabus4()
	{
        $file_name = 'Silabus Kelas 4.zip';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);
	}
 
	function silabus5()
	{
        $file_name = 'Silabus Kelas 5.zip';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);
	}
 
	function silabus6()
	{
        $file_name = 'Silabus Kelas 6.zip';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);
	}

	function ajar1()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/silabus/'.$file_name, null);

	}
 
	function ajar2()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/ajar/'.$file_name, null);
	}
 
	function ajar3()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/ajar/'.$file_name, null);
	}
 
	function ajar4()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/ajar/'.$file_name, null);
	}
 
	function ajar5()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/ajar/'.$file_name, null);
	}
 
	function ajar6()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/ajar/'.$file_name, null);
	}

	function uji1()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/uji/'.$file_name, null);

	}
 
	function uji2()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/uji/'.$file_name, null);
	}

    function uji3()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/uji/'.$file_name, null);
	}

    function uji4()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/uji/'.$file_name, null);
	}

    function uji5()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/uji/'.$file_name, null);
	}

    function uji6()
	{
        $file_name = 'kelas1.pdf';
        force_download(FCPATH.'/assets/uji/'.$file_name, null);
	}


}