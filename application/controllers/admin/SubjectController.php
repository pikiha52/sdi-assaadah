<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubjectController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('kelas');
		$this->load->model('subjects');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{	
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$subject = $this->subjects->selectAll(); 
		// var_dump(json_encode($subject));
		// die;
		$year = date('Y');
		$data = [
			'title' => 'Data Mata Pelajaran',
			'year' => $year,
			'subject' => $subject,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/subject/index');
		$this->load->view('template/footer');
	}

	public function add()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$kelas = $this->kelas->getall();
		$year = date('Y');
		$data = [
			'title' => 'Tambah Data Mata Pelajaran',
			'year' => $year,
			'kelas' => $kelas,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/subject/add');
		$this->load->view('template/footer');
	}

	public function store()
	{
		$kelas_id = $this->input->post('kelas_id');
		$name = $this->input->post('name');
		$point = $this->input->post('point_kkm');

		$data = array(
			'kelas_id' => $kelas_id,
			'name' => $name,
			'point_kkm' => $point
		);

		$store = $this->subjects->store('matpel', $data);

		if ($store) {

			$this->session->set_flashdata('succcess', 'message_success');
			redirect(base_url('admin/SubjectController'));

		}else {
			
			$this->session->set_flashdata('succcess', 'message_success');
			redirect(base_url('admin/SubjectController'));

		}
	}


}
