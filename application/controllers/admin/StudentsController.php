<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentsController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('students');
		$this->load->model('users');
		$this->load->model('teachers');
		$this->load->model('kelas');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{	
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$students = $this->students->selectregisAdmin();
		$year = date('Y');
		$data = [
			'title' => 'PPDB ONLINE',
			'year' => $year,
			'students' => $students,
			'name' => $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/student/index');
		$this->load->view('template/footer');
	}

	public function studActive()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$students = $this->students->studAct();

		// var_dump($students);
		// die;
		$year = date('Y');
		$data = [
			'title' => 'STUDENTS ACTIVE IN SCHOOLS',
			'year' => $year,
			'students' => $students,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/student/active');
		$this->load->view('template/footer');
	}

	public function updateStatus()
	{
		$id = $this->input->post('id');
		$nis = $this->students->getnis();
		$lastStudents = $this->students->updateStatus($id)->row();
		$status = $this->input->post('status');
		$data = array(
			'status' => $status,
		);

			$user_id = $lastStudents->users_id;
			$dataStud = array(
				'student_id' => $id,
				'nis' => $nis,
				'users_id' => $user_id
			);

		$store = $this->students->store('student', $dataStud);
		$query = $this->db->update('student_register', $data, array('id' => $id));

		if ($query) {

			$this->session->set_flashdata('success', 'message_success');
			redirect(base_url('admin/StudentsController/addStudent/'. $id));      

		} else {
			
			$this->session->set_flashdata('error', 'message_error');
			redirect(base_url('admin/StudentsController/addStudent/'. $id));   

		}
	}


	public function addStudent($id)
	{
		$id_user = $this->session->userdata('id');
		$name = $this->users->check($id_user)->row();
		$students = $this->students->byUpdateStatus($id);
		$kelas = $this->kelas->getall();
		$year = date('Y');
		$data = [
			'title' => 'STUDENTS ACTIVE IN SCHOOLS',
			'year' => $year,
			'kelas' => $kelas,
			'students' => $students,
			'name' => $name
		];


		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/student/add');
		$this->load->view('template/footer');

	}

	public function store()
	{
		$id = $this->input->post('id');
		$kelas_id = $this->input->post('kelas_id');

		$data = array(
			'kelas_id' => $kelas_id
		);


		$query = $this->db->update('student', $data, array('student_id' => $id));
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('admin/StudentsController/studActive'));   

	}

	public function update($id)
	{
		$year = date('Y');
		$id_user = $this->session->userdata('id');
		$name = $this->users->check($id_user)->row();
		$student = $this->students->byUpdateStatus($id);
		$kelas = $this->kelas->getall();
		$data = [
			'title' => 'Update Students',
			'name' => $name,
			'students' => $student,
			'kelas' => $kelas,
			'year' => $year
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/student/update');
		$this->load->view('template/footer');

	}

	public function updatestore()
	{
		$stud_id = $this->input->post('id');
		$stud_nis = $this->input->post('nis');
		$stud_class = $this->input->post('kelas_id');

		$data = array(
			'id' => $stud_id,
			'nis' => $stud_nis,
			'kelas_id' => $stud_class
		);

		$query = $this->db->update('student', $data, array('id' => $stud_id));
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('admin/StudentsController/studActive')); 
	}

}
