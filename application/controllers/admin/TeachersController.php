<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeachersController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('teachers');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$teachers = $this->teachers->selectAll();
		$year = date('Y');
		$data = [
			'title' => "Data Guru",
			'year' => $year,
			'name'	=> $name,
			'teachers' => $teachers
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/teachers/index');
		$this->load->view('template/footer');

	}


}
