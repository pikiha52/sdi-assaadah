<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usersController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('teachers');
		$this->load->model('users');
		$this->load->model('kelas');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$year = date('Y');
		$users = $this->users->selectAll();
		$data = [
			'title' => 'MANAGEMENT USERS',
			'year' => $year,
			'users' => $users,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/management-user/index');
		$this->load->view('template/footer');
	}

	public function add()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$year = date('Y');
		$data = [
			'title' => 'ADD USERS',
			'year' => $year,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/management-user/add');
		$this->load->view('template/footer');
	}

	public function store()
	{
		// config untuk upload gambar
		$config['upload_path']          = './assets/image/profile/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image');


		$codeTeach = $this->teachers->getCode();
		$today = date('Y-m-d');
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$pass = $this->input->post('password');
		$password = password_hash($pass, PASSWORD_DEFAULT);
		$status = $this->input->post('status');

		// jika status nya users maka akan mengeksekusi printah ini
		if ($status == 'teacher') {
			$_data = array('upload_data' => $this->upload->data());
			$data = array(
				'image' => $_data['upload_data']['file_name'],
				'username' => $username,
				'name' =>$name,
				'phone' => $phone,
				'address' => $address,
				'status' => $status,
				'password' => $password,
				'created_at' => $today
			);

			$store = $this->auth->store("users", $data);

			$getId = $this->users->getLastId();
			foreach($getId as $users):
				$user_id = $users['id'];
			endforeach;

			$teacher = array(
				'users_id' => $user_id,
				'code_teacher' => $codeTeach,
				'name' => $name,
				'place_ofbirth' => $this->input->post('place_ofbirth'),
				'date_birth' => $this->input->post('date_birth'),
				'education' => $this->input->post('education'),
				'created_at' => $today
			);

			$store = $this->auth->store("teachers", $teacher);

			if ($store) {

				$this->session->set_flashdata('succcess', 'message_success');
				redirect(base_url('admin/UsersController'));

			} else {

				$this->session->set_flashdata('error', 'message_error');
				redirect(base_url('admin/UsersController'));

			}

			// jika status nya selain user akan mengeksekusi perintah ini
		} else {

			$data = array(
				'username' => $username,
				'name' =>$name,
				'phone' => $phone,
				'address' => $address,
				'image' => 'undraw_profile.svg',
				'status' => $status,
				'password' => $password,
				'created_at' => $today
			);
			

			$store = $this->auth->store("users", $data);

			if ($store) {

				$this->session->set_flashdata('succcess', 'message_success');
				redirect(base_url('admin/UsersController'));

			} else {

				$this->session->set_flashdata('error', 'message_error');
				redirect(base_url('admin/UsersController'));

			}
		}
	}

	public function update()
	{
		$id = $this->input->post('id');
		$config['upload_path']          = './assets/image/profile/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$status = $this->input->post('status');

		$this->load->library('upload', $config);
	  
		if (!$this->upload->do_upload('image')){
	  
		 $data = array(
			'username' => $username,
			'name' =>$name,
			'phone' => $phone,
			'address' => $address,
			'status' => $status,
		);
	  
		 $query = $this->db->update('users', $data, array('id' => $id));;
		 $this->session->set_flashdata('succcess', 'message_success');
		 redirect(base_url('admin/UsersController'));     
	  
	   }else{
	
		$_data = array('upload_data' => $this->upload->data());
		$data = array(
		  'username' => $username,
		  'name' =>$name,
		  'phone' => $phone,
		  'address' => $address,
		  'status' => $status,
		  'image' => $_data['upload_data']['file_name']
		);
		// var_dump($data);
		// die();
		$query = $this->db->update('users', $data, array('id' => $id));;
		$this->session->set_flashdata('succcess', 'message_success');
		redirect(base_url('admin/UsersController'));     
	  }
	}

	public function destroy($id)
	{
		$check = $this->users->check($id)->row();
		if ($check->status == 'teacher') {

			$this->users->destroy($id);
			$this->session->set_flashdata('success', 'message_success');
			redirect(base_url('admin/UsersController'));

		} else {
			
			$this->users->destroyWali($id);
			$this->session->set_flashdata('success', 'message_success');
			redirect(base_url('admin/UsersController'));

		}
	}

}
