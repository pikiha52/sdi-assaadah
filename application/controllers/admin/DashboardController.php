<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('teachers');
		$this->load->model('users');
		$this->load->model('students');
		$this->load->model('subjects');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$stud = $this->students->studAct();
		$subj = $this->subjects->countSubj();
		$regis = $this->students->selectregisAdmin();
		$teachers = $this->teachers->selectAll();
		// var_dump($name);
		// die;
		$countteach = count($teachers);
		$countstud = count($stud);
		$countregis = count($regis);
		$countmatpel = count($subj);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'countteach' => $countteach,
			'countstud' => $countstud,
			'countregis' => $countregis,
			'countmatpel' => $countmatpel,
			'name'		=> $name
			
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-admin/dashboard/index');
		$this->load->view('template/footer');

	}



}
