<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function index()
	{
		$data = [
			'title' => "Login SD ISLAM ASSA'ADAH"
		];

		$this->load->view('frontend/login/index');
	}

	public function verivLogin()
	{
		// $this->form_validation->set_rules('nip', 'Nip', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) {

			$errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
			redirect('welcome');

		} else {

			$username = htmlspecialchars($this->input->post('username'));
			$password = htmlspecialchars($this->input->post('password'));
			$verivLogin = $this->auth->verivLogin($username)->row();

			if ($verivLogin == FALSE) {

				$this->session->set_flashdata('error', 'message_error');
				redirect(base_url('welcome'));

			} else {

				if (password_verify($password, $verivLogin->password)) {
					$this->session->set_userdata(['user_logged' => $verivLogin]);
					$this->session->set_userdata('id', $verivLogin->id);
					$this->session->set_userdata('status', $verivLogin->status);

					if ($verivLogin->status == 'admin') {
						
						$this->session->set_flashdata('success', 'message_success');
						redirect(base_url('admin/DashboardController'));

					} elseif ($verivLogin->status == 'teacher') {
						
						$this->session->set_flashdata('success', 'message_success');
						redirect(base_url('teacher/DashboardController'));

					} else {

						$this->session->set_flashdata('success', 'message_success');
						redirect(base_url('wali/DashboardController'));

					}
				} else {

					$this->session->set_flashdata('error', 'message_error');
					redirect(base_url('welcome'));

				}
			}
		}
	}

	public function storeRegis()
	{
		$today = date('Y-m-d');
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$pass = $this->input->post('password');
		$password = password_hash($pass, PASSWORD_DEFAULT);
		$status = $this->input->post('status');

			$data = array(
				'username' => $username,
				'name' =>$name,
				'phone' => $phone,
				'address' => $address,
				'image' => 'undraw_profile.svg',
				'status' => $status,
				'password' => $password,
				'created_at' => $today
			);

			$store = $this->auth->store("users", $data);

	
			if ($store) {

				$this->session->set_flashdata('success', 'message_success');
				redirect(base_url('welcome'));

			} else {

				$this->session->set_flashdata('error', 'message_error');
				redirect(base_url('welcome'));

			}

		}


	public function signout()
	{
		$this->session->sess_destroy();
		
		echo'<script>
        alert("Sukses! Anda berhasil logout."); 
        window.location.href="'.base_url('welcome').'";
        </script>';
	}

}
