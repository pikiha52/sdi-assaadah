<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('child');
		$this->load->model('students');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{
		$id = $this->session->userdata('id');
		$name = $this->users->check($id)->row();
		$childs = $this->child->selectAll($id)->row();
		$students = $this->students->selectRegister($id)->row();

		if ($childs == NULL) {
			
			$today = date('Y-m-d');
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'id' => $id,
				'name' => $name,
				'year' => $year,
				'students' => $students
			];
	
			$this->load->view('template/header', $data);
			$this->load->view('template/sidebar-calon');
			$this->load->view('template/topbar');
			$this->load->view('backend-wali/dashboard/index');
			$this->load->view('template/footer');

		} else if ($students->status == 'diproses') {
			
			$bukti_tf = $students->bukti_tf;
			$stud_id = $students->id;
			$today = date('Y-m-d');
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'id' => $id,
				'name' => $name,
				'year' => $year,
				'students' => $students,
				'bukti_tf' => $bukti_tf,
				'stud_id' => $stud_id
			];
	
			$this->load->view('template/header', $data);
			$this->load->view('template/sidebar-calon');
			$this->load->view('template/topbar');
			$this->load->view('backend-wali/dashboard/index');
			$this->load->view('template/footer');

		} else {

			$stud_id = $childs->student_id;
			$teach_id = $childs->kelas_id;
			$today = date('Y-m-d');
			$getAbsen = $this->child->showAbsent($stud_id, $teach_id, $today);
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'year' => $year,
				'id' => $id,
				'students' => $students,
				'getAbsen' => $getAbsen,
				'name'	=> $name
			];
	
	
			$this->load->view('template/header', $data);
			$this->load->view('template/sidebar');
			$this->load->view('template/topbar');
			$this->load->view('backend-wali/dashboard/index');
			$this->load->view('template/footer');
	

		}
		
	}

	public function store()
	{

		$today = date('Y-m-d');
		$old = $this->input->post('umur');
		$penghasilan = $this->input->post('penghasilan');
			// jika umur murid kurang dari 7 tahun tidak akan bisa
			// daftar
		if ($old < 7) {

			$this->session->set_userdata('error', 'message_error_old');
			redirect(base_url('wali/DashboardController'));
		
		} else {

			$data = array(
				'users_id' => $this->input->post('user_id'),
				'nama_lengkap' => $this->input->post('nama_lengkap'),
				'nama_p' => $this->input->post('nama_p'),
				'umur' => $old,
				'jen_kel' => $this->input->post('gender'),
				'tempat_lahir' => $this->input->post('tempat'),
				'tgl_lahir' => $this->input->post('tgl_lahir'),
				// data orang tua
				'nama_a' => $this->input->post('nama_a'),
				'nama_i' => $this->input->post('nama_i'),
				'pekerjaan_a' => $this->input->post('pekerjaan_a'),
				'pekerjaan_i' => $this->input->post('pekerjaan_i'),
				'penghasilan' => $penghasilan,
				'status' => 'diproses',
				'created_at' => $today
			);
	 
			$insert = $this->auth->store('student_register',$data);
			redirect('wali/DashboardController');	
		}

	  }


	public function uploadTF()
	{
		$id = $this->input->post('id');
		$config['upload_path']          = './assets/document/';
		$config['allowed_types']        = 'png|jpg|jpeg';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
		$this->load->library('upload', $config);
		$this->upload->do_upload('bukti_tf');

		$_data = array('upload_data' => $this->upload->data());
		$data = array(
			'bukti_tf' => $_data['upload_data']['file_name']
		);


		$query = $this->db->update('student_register', $data, array('id' => $id));
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('wali/dashboardcontroller'));   

	}
	

}
