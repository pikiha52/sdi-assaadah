<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChildController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('child');
		$this->load->model('users');
		$this->load->model('auth');
		if ($this->auth->notLogin()) redirect(site_url('welcome'));
	}

	public function index()
	{
		$user_id = $this->session->userdata('id');
		$name = $this->users->check($user_id)->row();
		$childs = $this->child->selectAll($user_id)->row();
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'childs' => $childs,
			'name'	=> $name
		];

		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-wali/data-child-absent/index');
		$this->load->view('template/footer');

	}

	public function showAbsent($stud_id)
	{
		
		$user_id = $this->session->userdata('id');
		$name = $this->users->check($user_id)->row();
		$childs = $this->child->selectAll($user_id)->row();
		$teach_id = $childs->kelas_id;
		$today = date('Y-m-d');
		$getAbsen = $this->child->showAbsent($stud_id, $teach_id, $today);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'getAbsen' => $getAbsen,
			'stud_id' => $stud_id,
			'name'	=> $name
		];
		
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-wali/data-child-absent/detail');
		$this->load->view('template/footer');

	}

	public function byDate($stud_id)
	{
		$date =	$this->input->post('date');
		if ($date == NULL) {
		
			$user_id = $this->session->userdata('id');
			$name = $this->users->check($user_id)->row();
			$childs = $this->child->selectAll($user_id)->row();
			$teach_id = $childs->kelas_id;
			$today = date('Y-m-d');
			$getAbsen = $this->child->showAbsent($stud_id, $teach_id, $today);
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'year' => $year,
				'getAbsen' => $getAbsen,
				'stud_id' => $stud_id,
				'name'	=> $name
			];
		} else {
		
			$user_id = $this->session->userdata('id');
			$name = $this->users->check($user_id)->row();
			$childs = $this->child->selectAll($user_id)->row();
			$teach_id = $childs->kelas_id;
			$getAbsen = $this->child->showAbsent($stud_id, $teach_id, $date);
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'year' => $year,
				'getAbsen' => $getAbsen,
				'stud_id' => $stud_id,
				'name' => $name
			];

		}
		
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-wali/data-child-absent/detail');
		$this->load->view('template/footer');

	}

	public function showPoint($stud_id)
	{
		
		$user_id = $this->session->userdata('id');
		$name = $this->users->check($user_id)->row();
		$childs = $this->child->selectAll($user_id)->row();
		$teach_id = $childs->kelas_id;
		$today = date('Y-m-d');
		$getPoint = $this->child->showPoint($stud_id, $teach_id, $today);
		$year = date('Y');
		$data = [
			'title' => "WELCOME TO SDI ASSA'ADAH",
			'year' => $year,
			'getPoint' => $getPoint,
			'stud_id' => $stud_id,
			'name'	=> $name
		];

		// var_dump($getPoint);
		// die();
		
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-wali/data-child-point/index');
		$this->load->view('template/footer');

	}

	public function byDatePoint($stud_id)
	{
		$date =	$this->input->post('date');
		if ($date == NULL) {
		
			$user_id = $this->session->userdata('id');
			$name = $this->users->check($user_id)->row();
			$childs = $this->child->selectAll($user_id)->row();
			$teach_id = $childs->kelas_id;
			$today = date('Y-m-d');
			$getPoint = $this->child->showPoint($stud_id, $teach_id, $today);
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'year' => $year,
				'getPoint' => $getPoint,
				'stud_id' => $stud_id,
				'name'	=> $name
			];
		} else {
		
			$user_id = $this->session->userdata('id');
			$name = $this->users->check($user_id)->row();
			$childs = $this->child->selectAll($user_id)->row();
			$teach_id = $childs->kelas_id;
			$getPoint = $this->child->showPoint($stud_id, $teach_id, $date);
			$year = date('Y');
			$data = [
				'title' => "WELCOME TO SDI ASSA'ADAH",
				'year' => $year,
				'getPoint' => $getPoint,
				'stud_id' => $stud_id,
				'name' => $name		
			];

		}
		
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar');
		$this->load->view('template/topbar');
		$this->load->view('backend-wali/data-child-point/index');
		$this->load->view('template/footer');

	}



}
