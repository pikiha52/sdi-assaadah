            <!-- Footer -->
            <footer class="sticky-footer bg-white">
            	<div class="container my-auto">
            		<div class="copyright text-center my-auto">
            			<span>Copyright &copy; SDI ASSA'ADAH <?= $year ?></span>
            		</div>
            	</div>
            </footer>
            <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
            	<i class="fas fa-angle-up"></i>
            </a>

			<script>
    		//sweetalert for success or error message
    		<?php if($this->session->flashdata('success') == 'message_success'):?>
    		    swal({
    		        type: "success",
    		        icon: "success",
    		        title: "Berhasil",
    		        text: "Selamat anda berhasil! ",
    		        timer: 1800,
    		        showConfirmButton: false,
    		        showCancelButton: false,
    		        buttons: false,
    		    });
    		    <?php elseif($this->session->flashdata('error') == 'message_error'):?>
    		        swal({
    		            type: "error",
    		            icon: "error",
    		            title: "Gagal!",
    		            text: " Maaf terjadi kesalahan!",
    		            timer: 1800,
    		            showConfirmButton: false,
    		            showCancelButton: false,
    		            buttons: false,
    		        });
				<?php elseif($this->session->flashdata('error') == 'message_error_old'):?>
    		        swal({
    		            type: "error",
    		            icon: "error",
    		            title: "Gagal!",
    		            text: " Umur anak anda kurang dari 7 Tahun!",
    		            timer: 1800,
    		            showConfirmButton: false,
    		            showCancelButton: false,
    		            buttons: false,
    		        });
    		    <?php endif; ?>
    		</script>


            <!-- Bootstrap core JavaScript-->
            <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
            <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

            <!-- Core plugin JavaScript-->
            <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

            <!-- Custom scripts for all pages-->
            <script src="<?php echo base_url('assets/js/sb-admin-2.min.js') ?>"></script>

            <!-- Core plugin Datatables -->
            <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"> </script>
            <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

            </body>

            </html>
