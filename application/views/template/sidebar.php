    <!-- Page Wrapper -->
    <div id="wrapper">

    	<!-- Sidebar -->
    	<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    		<!-- Sidebar - Brand -->

			<!-- <img src="<?php echo base_url('assets/image/content/logo.jpeg') ?>" height="180" alt=""> -->
    		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
			SDI ASSA'ADAH</a>

    		<?php if($this->session->userdata('status') == 'admin') :?>
    		<!-- Divider -->
    		<hr class="sidebar-divider my-0">

    		<!-- Nav Item - Dashboard -->
    		<li class="nav-item active">
    			<a class="nav-link" href="<?php echo base_url('admin/DashboardController') ?>">
    				<i class="fas fa-fw fa-tachometer-alt"></i>
    				<span>DASHBOARD</span></a>
    		</li>

    		<!-- Divider -->
    		<hr class="sidebar-divider">

    		<!-- Heading -->
    		<div class="sidebar-heading">
    			Management users
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFor"
    				aria-expanded="true" aria-controls="collapseFor">
    				<i class="fas fa-fw fa-user"></i>
    				<span>USERS</span>
    			</a>
    			<div id="collapseFor" class="collapse" aria-labelledby="headingFor" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<h6 class="collapse-header">DATA USERS:</h6>
    					<a class="collapse-item" href="<?php echo base_url('admin/UsersController/add') ?>">TAMBAH USER</a>
    					<a class="collapse-item" href="<?php echo base_url('admin/UsersController') ?>">USERS</a>
    					<!-- <a class="collapse-item" href="cards.html">All Students</a> -->
    				</div>
    			</div>
    		</li>

    		<!-- Divider -->
    		<hr class="sidebar-divider">

    		<!-- Heading -->
    		<div class="sidebar-heading">
    			SISWA
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
    				aria-expanded="true" aria-controls="collapseTwo">
    				<i class="fas fa-fw fa-user"></i>
    				<span>SISWA</span>
    			</a>
    			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<h6 class="collapse-header">DATA SISWA:</h6>
    					<a class="collapse-item" href="<?php echo base_url('admin/StudentsController') ?>">PPDB ONLINE</a>
    					<a class="collapse-item"
    						href="<?php echo base_url('admin/StudentsController/studActive') ?>">SISWA AKTIF</a>
    					<!-- <a class="collapse-item" href="cards.html">All Students</a> -->
    				</div>
    			</div>
    		</li>

    		<!-- Divider -->
    		<hr class="sidebar-divider d-none d-md-block">


    		<!-- Heading -->
    		<div class="sidebar-heading">
    			GURU
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne"
    				aria-expanded="true" aria-controls="collapseOne">
    				<i class="fas fa-fw fa-users"></i>
    				<span>GURU</span>
    			</a>
    			<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<h6 class="collapse-header">DATA GURU:</h6>
    					<!-- <a class="collapse-item" href="<?php echo base_url('admin/TeachersController/add') ?>">Add Teachers</a> -->
    					<a class="collapse-item" href="<?php echo base_url('admin/TeachersController') ?>">GURU</a>
    				</div>
    			</div>
    		</li>


    		<!-- Divider -->
    		<hr class="sidebar-divider d-none d-md-block">


    		<!-- Heading -->
    		<div class="sidebar-heading">
    			MATA PELAJARAN
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTree"
    				aria-expanded="true" aria-controls="collapseTree">
    				<i class="fas fa-fw fa-users"></i>
    				<span>MATA PELAJARAN</span>
    			</a>
    			<div id="collapseTree" class="collapse" aria-labelledby="headingTree" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<h6 class="collapse-header">Data MATA PELAJARAN:</h6>
    					<a class="collapse-item" href="<?php echo base_url('admin/SubjectController/add') ?>">TAMBAH</a>
    					<a class="collapse-item" href="<?php echo base_url('admin/SubjectController') ?>">MATA PELAJARAN</a>
    				</div>
    			</div>
    		</li>

    		<!-- Divider -->
    		<hr class="sidebar-divider d-none d-md-block">
    		<?php elseif($this->session->userdata('status') == 'teacher') :?>

    		<!-- Divider -->
    		<hr class="sidebar-divider my-0">

    		<!-- Nav Item - Dashboard -->
    		<li class="nav-item active">
    			<a class="nav-link" href="href="<?php echo base_url('teacher/DashboardController') ?>"">
    				<i class="fas fa-fw fa-tachometer-alt"></i>
    				<span>DASHBOARD</span></a>
    		</li>

    		<!-- Divider -->
    		<hr class="sidebar-divider">

    		<!-- Heading -->
    		<div class="sidebar-heading">
    			Management Students
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFor"
    				aria-expanded="true" aria-controls="collapseFor">
    				<i class="fas fa-fw fa-user"></i>
    				<span>SISWA</span>
    			</a>
    			<div id="collapseFor" class="collapse" aria-labelledby="headingFor" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<a class="collapse-item" href="<?php echo base_url('teacher/StudentsController/addAbsent') ?>">TAMBAH ABSEN SISWA</a>
    					<a class="collapse-item" href="<?php echo base_url('teacher/StudentsController/addPoint') ?>">TAMBAH NILAI SISWA</a>
    					<!-- <a class="collapse-item" href="cards.html">All Students</a> -->
    				</div>
    			</div>
    		</li>

    		<!-- Divider -->
    		<hr class="sidebar-divider">

    		<!-- Heading -->
    		<div class="sidebar-heading">
    			Details
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive"
    				aria-expanded="true" aria-controls="collapseFive">
    				<i class="fas fa-fw fa-user"></i>
    				<span>DETAIL</span>
    			</a>
    			<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<a class="collapse-item" href="<?php echo base_url('teacher/DetailStudentsAbsentController') ?>">ABSEN SISWA</a>
    					<a class="collapse-item" href="<?php echo base_url('teacher/DetailStudentsPointController') ?>">NILAI SISWA</a>
    					<!-- <a class="collapse-item" href="cards.html">All Students</a> -->
    				</div>
    			</div>
    		</li>

    		<?php else :?>

    		<!-- Divider -->
    		<hr class="sidebar-divider my-0">

    		<!-- Nav Item - Dashboard -->
    		<li class="nav-item active">
    			<a class="nav-link" href="<?php echo base_url('wali/DashboardController') ?>">
    				<i class="fas fa-fw fa-tachometer-alt"></i>
    				<span>DASHBOARD</span></a>
    		</li>
			
    		<!-- Divider -->
    		<hr class="sidebar-divider">

    		<!-- Heading -->
    		<div class="sidebar-heading">
    			Management Students
    		</div>

    		<!-- Nav Item - Pages Collapse Menu -->
    		<li class="nav-item">
    			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFor"
    				aria-expanded="true" aria-controls="collapseFor">
    				<i class="fas fa-fw fa-user"></i>
    				<span>PANTAU ANAK</span>
    			</a>
    			<div id="collapseFor" class="collapse" aria-labelledby="headingFor" data-parent="#accordionSidebar">
    				<div class="bg-white py-2 collapse-inner rounded">
    					<a class="collapse-item" href="<?php echo base_url('wali/ChildController') ?>">LIHAT DATA</a>
    				</div>
    			</div>
    		</li>


    		<?php endif ;?>

    		<!-- Sidebar Toggler (Sidebar) -->
    		<div class="text-center d-none d-md-inline">
    			<button class="rounded-circle border-0" id="sidebarToggle"></button>
    		</div>

    	</ul>
    	<!-- End of Sidebar -->
