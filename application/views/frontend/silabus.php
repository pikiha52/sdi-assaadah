<section id="silabus">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
				data-aos="fade-up">
				<div>
					<h4 style="text-align: center;" class="mb-4">DATA SILABUS</h4>
					<table class="table table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>KELAS</th>
								<th>DOWNLOAD</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>SILABUS KELAS 1</td>
								<td><a href="<?php echo base_url('Download/silabus1') ?>" class="btn btn-info btn-sm"><i
											class="bi bi-cloud-download icon-show"></i> Download</a></td>
							</tr>
							<tr>
								<td>SILABUS KELAS 2</td>
								<td><a href="<?php echo base_url('download/silabus2') ?>" class="btn btn-info btn-sm"><i
											class="bi bi-cloud-download icon-show"></i> Download</a></td>
							</tr>
							<tr>
								<td>SILABUS KELAS 3</td>
								<td><a href="<?php echo base_url('download/silabus3') ?>" class="btn btn-info btn-sm"><i
											class="bi bi-cloud-download icon-show"></i> Download</a></td>
							</tr>
							<tr>
								<td>SILABUS KELAS 4</td>
								<td><a href="<?php echo base_url('download/silabus4') ?>" class="btn btn-info btn-sm"><i
											class="bi bi-cloud-download icon-show"></i> Download</a></td>
							</tr>
							<tr>
								<td>SILABUS KELAS 5</td>
								<td><a href="<?php echo base_url('download/silabus5') ?>" class="btn btn-info btn-sm"><i
											class="bi bi-cloud-download icon-show"></i> Download</a></td>
							</tr>
							<tr>
								<td>SILABUS KELAS 6</td>
								<td><a href="<?php echo base_url('download/silabus6') ?>" class="btn btn-info btn-sm"><i
											class="bi bi-cloud-download icon-show"></i> Download</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</section>

<br><br><br><br><br><br>
<section id="kalender" class="about">
	<div class="container">

		<div class="row">
			<div class="col-lg-12" data-aos="zoom-in">
				<div class="content pt-4 pt-lg-0">
					<center>
						<h3>Kalender Akademik</h3>
						<img src="<?php echo base_url('assets/image/content/kalender pendidikan.jpeg') ?>"
							class="img-fluid" alt="">
					</center>
				</div>
			</div>
		</div>

	</div>
</section><!-- End About Section -->


<br><br><br><br><br><br>
<section id="ajar">
	<div class="container">

		<div class="row">
			<div class="col-lg-12" data-aos="zoom-in">
				<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
					data-aos="fade-up">
					<div>
						<h4 style="text-align: center;" class="mb-4">MATERI AJAR</h4>
						<table class="table table-striped table-bordered" width="100%">
							<thead>
								<tr>
									<th>KELAS</th>
									<th>DOWNLOAD</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>KELAS 1</td>
									<td><a href="<?php echo base_url('Download/ajar1') ?>"
											class="btn btn-info btn-sm"><i class="bi bi-cloud-download icon-show"></i>
											Download</a></td>
								</tr>
								<tr>
									<td>KELAS 2</td>
									<td><a href="<?php echo base_url('download/ajar2') ?>"
											class="btn btn-info btn-sm"><i class="bi bi-cloud-download icon-show"></i>
											Download</a></td>
								</tr>
								<tr>
									<td>KELAS 3</td>
									<td><a href="<?php echo base_url('download/ajar3') ?>"
											class="btn btn-info btn-sm"><i class="bi bi-cloud-download icon-show"></i>
											Download</a></td>
								</tr>
								<tr>
									<td>KELAS 4</td>
									<td><a href="<?php echo base_url('download/ajar4') ?>"
											class="btn btn-info btn-sm"><i class="bi bi-cloud-download icon-show"></i>
											Download</a></td>
								</tr>
								<tr>
									<td>KELAS 5</td>
									<td><a href="<?php echo base_url('download/ajar5') ?>"
											class="btn btn-info btn-sm"><i class="bi bi-cloud-download icon-show"></i>
											Download</a></td>
								</tr>
								<tr>
									<td>KELAS 6</td>
									<td><a href="<?php echo base_url('download/ajar6') ?>"
											class="btn btn-info btn-sm"><i class="bi bi-cloud-download icon-show"></i>
											Download</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</section><!-- End About Section -->


<br><br><br><br><br><br>
<section id="uji">
	<div class="container">

		<div class="row">
			<div class="col-lg-12" data-aos="zoom-in">
				<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
					data-aos="fade-up">
					<div>
						<h4 style="text-align: center;" class="mb-4">MATERI UJI</h4>
						<table class="table table-striped table-bordered" width="100%">
							<thead>
								<tr>
									<th>KELAS</th>
									<th>DOWNLOAD</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>KELAS 1</td>
									<td><a href="<?php echo base_url('Download/uji1') ?>" class="btn btn-info btn-sm"><i
												class="bi bi-cloud-download icon-show"></i> Download</a></td>
								</tr>
								<tr>
									<td>KELAS 2</td>
									<td><a href="<?php echo base_url('download/uji2') ?>" class="btn btn-info btn-sm"><i
												class="bi bi-cloud-download icon-show"></i> Download</a></td>
								</tr>
								<tr>
									<td>KELAS 3</td>
									<td><a href="<?php echo base_url('download/uji3') ?>" class="btn btn-info btn-sm"><i
												class="bi bi-cloud-download icon-show"></i> Download</a></td>
								</tr>
								<tr>
									<td>KELAS 4</td>
									<td><a href="<?php echo base_url('download/uji4') ?>" class="btn btn-info btn-sm"><i
												class="bi bi-cloud-download icon-show"></i> Download</a></td>
								</tr>
								<tr>
									<td>KELAS 5</td>
									<td><a href="<?php echo base_url('download/uji5') ?>" class="btn btn-info btn-sm"><i
												class="bi bi-cloud-download icon-show"></i> Download</a></td>
								</tr>
								<tr>
									<td>KELAS 6</td>
									<td><a href="<?php echo base_url('download/uji5') ?>" class="btn btn-info btn-sm"><i
												class="bi bi-cloud-download icon-show"></i> Download</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
