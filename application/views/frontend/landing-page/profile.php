<br><br>
<div class="container marketing">
	<div class="row mb-5">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h5>Visi dan Misi</h5>
				</div>
				<div class="card-body">
					<blockquote class="blockqoute">
						<p>TERWUJUDNYA PEMBENTUKAN GENERASI ISLAM
							BERKUALITAS, BERIMAN, BERTAOWA, CERDAS,
							TERAMPIL, MANDIRI, BERBUDAYA SERTA
							BERWAWASAN LINGKUNGAN</p>
						<p>
							Misi Dasar Islam Assa'adah Cipinang:<br>
							MEMBANTU PEMERINTAH DALAM MEMBENTUK
							BANGSA YANG BERPENDIDIKDN DAN BERMORAL dengan upaya:<br>
							<ul>
								<li>Menanamkan keyakinan akitah melalui pengamalan
									ajaran agama</li>
								<li>Mengoptimalkan proses pembelajaran dan bimbingan.</li>
								<li>Mengembangkan pengetahuan di bidang ilmu dan
									teknologi , bahasa, olahraga dan seni” budaya sesuai
									dengan bakat, minat dan potensi peserta didik.</li>
								<li>Membina kemandirian peserta didik melalui kegiatan
									pembiasaan dan pengembangan diri yang terencana dan perduli
									terhadap kelestarian lingkungan hidup</li>
								<li>Menjalin kerjasama bidang pendidikan yang Harmonis
									antara Warga sekolah, Pakar pendidikan, dan kerjasama
									dengan instansi lintas sektoral untuk menciptakan
									lingkungan sekolah yang asri,aman,nyaman, hijau dan
									kondusif</li>
								<li>Membiasakan Pemanfaatan Teknologi Informasi dalam Kegiatan Belajar Mengajar
									dengan Berpedoman Pada Aspek Ramah Lingkungan.
								</li>
							</ul>
						</p>
					</blockquote>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h5>Tujuan Pendidikan</h5>
				</div>
				<div class="crad-body">
					<blockquote class="blockqoute">
						<p> Tujuan pendidikan adalah meletakkan
							pengetahuan, kepribadian, akhlak mulia, serta
							kecerdasan,
							keterampilan untuk hidup mandiri dan mengikuti pendidikar
							lebih lanjut</p>
						<p>Mengacu pada visi dan misi sekolah, serta tujuan umum
							pendidikan dasar, tujuan sekolah dalam mengembangkan
							pendidikan ini adalah sebagai berikut ini</p>

						<p>
							<ul>
								<li>Terbaik dalam pengembangan budaya sekolah yang religius melalui
									pembiasaan dan kegiatan keagamaan.
								</li>
								<li>Terbaik dalam pelaksanakan pembelajaran dengan pendekatan tematik
									integratif dan pendekatan Scientific melalui pembelajaran Pakem.
								</li>
								<li>Terbaik dalam kegiatan pembiasaan dan pengembangan
									diri yang terencana dan peduli terhadap kelestarian
									lingkungan hidup</li>
								<li>Mengembangkan berbagai pendekatan dan metode dalam
									proses belajar mengajar di kelas berbasis pendidikan
									budaya dan karakter bangsa.</li>
								<li>Menyelenggarakan berbagai kegiatan sosial yang menjadi
									bagian dari pendidikan budaya dan karakter bangsa.</li>
								<li>Menjalin kerja sama dengan lembaga pendidikan baik di
									dalam negeri maupun luar negeri.</li>
								<li>Memanfaatkan dan memelihara fasilitas sekolah untuk
									sebesar-besarnya mendukung proses belajar mengajar
									berbasis TIK.</li>
							</ul>
						</p>
					</blockquote>
				</div>

			</div>
		</div>
	</div>


	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">STRUKTU ORGANISASI SDI ASSA'ADAH</h6>
		</div>
		<div class="card-body">
			<img src="<?php echo base_url('assets/image/struktur_organisasi.jpeg') ?>" alt="">
		</div>
	</div>
</div>
