<br><br>
<div class="container marketing">

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text">DATA SISWA SDI-ASSA'ADAH</h6>
		</div>
		<div class="card-body">
			<table class="table table-striped" id="table" width="100%">
				<thead>
					<tr>
						<th>NIS</th>
						<th>NAMA SISWA</th>
						<th>NAMA WALI KELAS</th>
						<th>KODE GURU</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($siswa as $childs) :?>
					<tr>
						<td><?php echo $childs->nis ?></td>
						<td><?php echo $childs->nama_lengkap ?></td>
						<td><?php echo $childs->name ?></td>
						<td><?php echo $childs->code_teacher ?></td>
					</tr>
					<?php endforeach ;?>
				</tbody>
			</table>
		</div>
	</div>


</div>


<script>
	$(document).ready(function () {
		$('#table').DataTable({

		});
	});

</script>
