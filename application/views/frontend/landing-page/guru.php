<br><br>
<div class="container marketing">

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text">DATA GURU SDI-ASSA'ADAH</h6>
		</div>
		<div class="card-body">
				<table class="table table-striped" id="table" width="100%">
					<thead>
						<tr>
							<th></th>
							<th>KODE GURU</th>
							<th>NAMA LENGKAP</th>
							<th>PENDIDIKAN</th>
							<th>JABATAN</th>
							<th>TEMPAT DAN TANGGAL LAHIR</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($guru as $gu) :?>
						<tr>
							<td><img src="<?php echo base_url('assets/image/profile/'. $gu->image) ?>" height="50" alt=""></td>
							<td><?php echo $gu->code_teacher ?></td>
							<td><?php echo $gu->name ?></td>
							<td><?php echo $gu->education ?></td>
							<td><?php echo $gu->position ?></td>
							<td><?php echo $gu->place_ofbirth.' '.$gu->date_birth ?></td>
						</tr>
						<?php endforeach ;?>
					</tbody>
				</table>
		</div>
	</div>


</div>


                <script>
                	$(document).ready(function () {
                		$('#table').DataTable({
                			
                		});
                	});

                </script>
