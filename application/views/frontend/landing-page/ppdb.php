<br><br>
<div class="container marketing">

	<!-- Three columns of text below the carousel -->
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header bg-dark"><span style="color: white;">Panduan PPDB</span></div>
				<div class="card-body">
					<div class="accordion accordion-flush" id="accordionFlushExample">
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingOne">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
									data-bs-target="#flush-collapseOne" aria-expanded="false"
									aria-controls="flush-collapseOne">
									Penerimaan Peserta Didik Baru (PPDB) dilaksanakan dengan ketentuan sebagai berikut:
								</button>
							</h2>
							<div id="flush-collapseOne" class="accordion-collapse collapse"
								aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">Dinas Pendidikan dan sekolah diminta menyiapkan mekanisme PPDB 
									yang mengikuti protokol kesehatan untuk mencegah penyebaran Covid-19, 
									termasuk mencegah berkumpulnya siswa dan orangtua secara fisik di sekolah;</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingTwo">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
									data-bs-target="#flush-collapseTwo" aria-expanded="false"
									aria-controls="flush-collapseTwo">
									PPDB pada Jalur Prestasi dilaksanakan berdasarkan:
								</button>
							</h2>
							<div id="flush-collapseTwo" class="accordion-collapse collapse"
								aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">
									<li>akumulasi nilai rapor ditentukan berdasarkan nilai lima semester terakhir; dan/atau</li>
									<li>prestasi akademik dan non-akademik di luar rapor sekolah;</li>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingThree">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
									data-bs-target="#flush-collapseThree" aria-expanded="false"
									aria-controls="flush-collapseThree">
									Pusat data dan informasi
								</button>
							</h2>
							<div id="flush-collapseThree" class="accordion-collapse collapse"
								aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">Pusat Data dan Informasi (Pusdatin) Kementerian Pendidikan dan
									 Kebudayaan menyediakan bantuan teknis bagi daerah yang memerlukan mekanisme PPDB daring.</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingFor">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
									data-bs-target="#flush-collapseFor" aria-expanded="false"
									aria-controls="flush-collapseFor">
									Orangtua harus melakukan register dan melakukan login untuk mendaftarkan anak nya.
								</button>
							</h2>
							<div id="flush-collapseFor" class="accordion-collapse collapse"
								aria-labelledby="flush-headingFor" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">Setelah melakukan register, dan melakukan login orang tua akan mengisi form
									untuk mendaftarkan anaknya sekolah.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.col-lg-4 -->
	</div><!-- /.row -->




</div><!-- /.container -->
