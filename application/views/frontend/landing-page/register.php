<br><br>
<div class="container marketing">

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Register</h6>
		</div>
		<div class="card-body">
			<form action="<?php echo base_url('AuthController/storeRegis') ?>" method="POST">
				<div class="form-group col-md-12">
					<label for="username">username</label>
					<input type="text" class="form-control" name="username">
				</div>

				<div class="form-group col-md-12">
					<label for="lastname">Nama</label>
					<input type="text" class="form-control" name="name">
				</div>

				<div class="form-group col-md-12">
					<label for="lastname">Nomor Telefon</label>
					<input type="number" class="form-control" name="phone">
				</div>

				<div class="form-group col-md-12">
					<label for="lastname">Alamat</label>
					<input type="text" class="form-control" name="address">
				</div>

				<div class="form-group col-md-12">
					<label for="lastname">Password</label>
					<input type="text" class="form-control" name="password">
				</div>

				<div class="form-group col-md-12" hidden>
					<label for="code">Status</label>
					<select name="status" class="form-control" id="status">
						<option value="wali">Wali</option>
					</select>
				</div>

				<div class="card-footer">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>

</div>
