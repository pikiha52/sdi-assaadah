	<main>

		<div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
			<div class="carousel-indicators">
				<button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-current="true"
					aria-label="Slide 1"></button>
				<button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
			</div>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="<?php echo base_url('assets/image/unnamed.jpg') ?>" alt="">
					<div class="container">
						<div class="carousel-caption text-start">
							<h1>SD ISLAM ASSA'ADAH.</h1>
						</div>
					</div>
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/image/head.png') ?>" alt="">
					<div class="container">
						<div class="carousel-caption">

						</div>
					</div>
				</div>
			</div>
			<button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Previous</span>
			</button>
			<button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Next</span>
			</button>
		</div>


		<!-- Marketing messaging and featurettes
  ================================================== -->
		<!-- Wrap the rest of the page in another container to center all the content. -->

		<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<div class="row">
				<div class="col-lg-8">
					<div class="card">
						<div class="card-header bg-dark"><span style="color: white;">Sambutan Kepala Sekolah</span></div>
						<div class="card-body">
							<img src="<?php echo base_url('assets/image/profile/kepalasekolah.jpeg') ?>" height="300"
								alt="">
							<p>Assalamu’alaikum Wr. Wb</p>



							<p>Selamat datang di website resmi SD Islam Asssa'adah</p>

							<p>Puji dan syukur kami haturkan ke hadirat Allat SWT, atas berkat, rahmat dan ridhoNya maka
								website
								SD Pelita akhirnya bisa terwujud.</p>

							<p>Selamat berjumpa di SD ISLAM ASSA'ADAH Cipinang Jakarta Timur melalui media website ini,
								semoga
								melalui media ini kebutuhan dan layanan komunikasi dapat memenuhi harapan masyarakat dan
								pihak-pihak yang berkepentingan.</p>

							<p>Tujuan utama website ini adalah sebagai media informasi kepada masyarakat luas tentang
								keberadaan
								sekolah, sehingga sekolah menjadi lebih dikenal dan siap menerima masukan dan kritik
								membangun dari
								pihak luar demi terwujudnya visi - misi sekolah.</p>

							<p>Kami berharap website ini bisa memberikan manfaat yang seluas-luasnya kepada keluarga besar
								SD
								ISLAM ASSA'ADAH CIPINANG JAKARTA TIMUR, dan dunia pendidikan, serta masyarakat luas pada
								umumnya.</p>

							<p>Akhir kata, tak lupa saya ucapkan terima kasih kepada pengelola web yang telah bekerja keras
								demi
								terwujudnya web ini, serta seluruh guru, karyawan dan siswa SD ISLAM ASSA'ADAH sehingga
								website
								sekolah ini menjadi lebih berguna dan bermanfaat</p>



							<p>Kepala Sekolah</p>

							<p>Sri Dewi Retno Wulan, S.Pd</p>
						</div>
					</div>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<div class="card">
						<div class="card-header bg-dark"><span style="color: white;">Login</span></div>
						<div class="card-body">
							<main class="form-signin">
								<form action="<?php echo base_url('AuthController/verivLogin') ?>" method="post">
									<!-- <img class="mb-4" src="<?php echo base_url('assets/image/bootstrap-logo.svg') ?>"
										alt="" width="72" height="57"> -->
									<h1 class="h3 mb-3 fw-normal">Please sign in</h1>

									<label for="inputEmail" class="visually-hidden">Username</label>
									<input type="text" id="inputEmail" class="form-control" name="username"
										placeholder="Username or Code Teachers"><br>

									<label for="inputPassword" class="visually-hidden">Password</label>
									<input type="password" id="inputPassword" class="form-control" name="password"
										placeholder="Password" required>
									<br>

									<button class="w-100 btn btn-lg btn-info" type="submit">Sign in</button>
								</form>
							</main>
						</div>
					</div>
				</div><!-- /.col-lg-4 -->
			</div><!-- /.row -->




		</div><!-- /.container -->
