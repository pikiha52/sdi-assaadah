<section id="hero">

	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
				data-aos="fade-up">
				<div>
					<h4>DIREKTORI GURU</h4>
					<table class="table table-striped" id="table">
						<thead>
							<tr>
								<th></th>
								<th>KODE GURU</th>
								<th>NAMA LENGKAP</th>
								<th>PENDIDIKAN</th>
								<th>WALIKELAS / JABATAN</th>
								<th>TEMPAT DAN TANGGAL LAHIR</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($guru as $gu) :?>
							<tr>
								<td><img src="<?php echo base_url('assets/image/profile/'. $gu->image) ?>" height="50"
										alt=""></td>
								<td><?php echo $gu->code_teacher ?></td>
								<td><?php echo $gu->name ?></td>
								<td><?php echo $gu->education ?></td>
								<td><?php echo $gu->nama_kelas ?></td>
								<td><?php echo $gu->place_ofbirth.', '.$gu->date_birth ?></td>
							</tr>
							<?php endforeach ;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</section><!-- End Hero -->


<script>
	$(document).ready(function () {
		$('#table').DataTable({

		});
	});

</script>
