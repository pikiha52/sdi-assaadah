  	<!-- ======= F.A.Q Section ======= -->
  	<br><br>
  	<section id="petunjuk" class="faq">
  		<div class="container">

  			<div class="section-title" data-aos="fade-up">
  				<h2>Panduan Pendaftaran PPDB ONLINE di Sekolah Dasar Islam ASSA'ADAH</h2>
  			</div>

  			<ul class="faq-list">

  				<li>
  					<div data-bs-toggle="collapse" class="collapsed question" href="#faq1">Penerimaan Peserta Didik
  						Baru (PPDB) dilaksanakan dengan ketentuan sebagai berikut:
  						<i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq1" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							Dinas Pendidikan dan sekolah diminta menyiapkan mekanisme PPDB
  							yang mengikuti protokol kesehatan untuk mencegah penyebaran Covid-19,
  							termasuk mencegah berkumpulnya siswa dan orangtua secara fisik di sekolah
  						</p>
  					</div>
  				</li>

  				<li>
  					<div data-bs-toggle="collapse" href="#faq2" class="collapsed question"> PPDB pada Jalur Prestasi
  						dilaksanakan berdasarkan:
  						<i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq2" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							akumulasi nilai rapor ditentukan berdasarkan nilai lima semester terakhir<br>dan/atau
  							prestasi akademik dan non-akademik di luar rapor sekolah
  						</p>
  					</div>
  				</li>

  				<li>
  					<div data-bs-toggle="collapse" href="#faq3" class="collapsed question">Pusat data dan informasi<i
  							class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq3" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							Pusat Data dan Informasi (Pusdatin) Kementerian Pendidikan dan
  							Kebudayaan menyediakan bantuan teknis bagi daerah yang memerlukan mekanisme PPDB daring.
  						</p>
  					</div>
  				</li>

  				<li>
  					<div data-bs-toggle="collapse" href="#faq4" class="collapsed question">Orangtua harus melakukan
  						register
  						dan melakukan
  						login untuk mendaftarkan anak nya.<i class="bi bi-chevron-down icon-show"></i><i
  							class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq4" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							Setelah melakukan register, dan melakukan login orang tua akan mengisi form
  							untuk mendaftarkan anaknya sekolah.
  						</p>
  					</div>
  				</li>

  			</ul>

  		</div>
  	</section><!-- End Frequently Asked Questions Section -->


  	<br><br><br><br><br><br><br><br><br><br>
  	<!-- ======= Contact Section ======= -->
  	<section id="register" class="contact section-bg">
  		<div class="container">

  			<div class="section-title" data-aos="fade-up">
  				<h2>Register</h2>
  			</div>

  			<div class="row">

  				<div class="col-lg-12" data-aos="fade-left">
  					<form action="<?php echo base_url('AuthController/storeRegis') ?>" method="post">
  						<!-- <div class="row"> -->
  						<div class="form-group mt-3">
  							<label for="name">USERNAME</label>
  							<input type="text" class="form-control" name="username" required>
  						</div>
  						<div class="form-group mt-3">
  							<label for="name">NAMA LENGKAP</label>
  							<input type="text" class="form-control" name="name" required>
  						</div>
  						<!-- </div> -->
  						<div class="form-group mt-3">
  							<label for="name">NOMOR HANDPHONE (WA)</label>
  							<input type="number" class="form-control" name="phone" required>
  						</div>
  						<div class="form-group mt-3">
  							<label for="name">ALAMAT</label>
  							<input type="text" class="form-control" name="address" required>
  						</div>
  						<div class="form-group mt-3">
  							<label for="name">PASSWORD</label>
  							<input type="password" class="form-control" name="password" required>
  						</div>
  						<div class="form-group col-md-12 mb-2" hidden>
  							<label for="code">Status</label>
  							<select name="status" class="form-control" id="status">
  								<option value="wali">Wali</option>
  							</select>
  						</div><br>
  						<button class="btn btn-primary btn-lg" type="submit">LOGIN</button>
  					</form>
  				</div>

  			</div>

  		</div>
  	</section><!-- End Contact Section -->
