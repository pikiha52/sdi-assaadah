  <!-- ======= Hero Section ======= -->
  <section id="hero">

  	<div class="container">
  		<div class="row">
  			<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
  				data-aos="fade-up">
  				<center>
					  <div>
					  <img src="<?php echo base_url('assets/image/content/logo.jpeg') ?>" alt="">
						  <h1>SD Islam Assa'adah</h1>
						  <h2>Jl Madrasah No.14 RT004/RW0011, Kel.Cipinang, Kel.Pulogadung, Kota Jakarta Timur</h2>
						  <a href="#login" class="btn-get-started scrollto">login?</a>
					  </div>
				  </center>
  			</div>
  		</div>
  	</div>

  </section><!-- End Hero -->

  <main id="main">

  	<!-- ======= About Section ======= -->
  	<section id="about" class="about">
  		<div class="container">

  			<div class="row">
  				<div class="col-lg-6" data-aos="zoom-in">
  					<img src="<?php echo base_url('assets/image/profile/kepalasekolah.jpeg') ?>" class="img-fluid"
  						alt="">
  				</div>
  				<div class="col-lg-6 d-flex flex-column justify-contents-center" data-aos="fade-left">
  					<div class="content pt-4 pt-lg-0">
  						<h3>Sambutan Kepala Sekolah</h3>
  						<p class="fst-italic">
  							Assalamu’alaikum Wr. Wb</p>
  						<p>Selamat datang di website resmi SD Islam Asssa'adah</p>
  						<p>Puji dan syukur kami haturkan ke hadirat Allat SWT, atas berkat, rahmat dan ridhoNya maka
  							website
  							SD Pelita akhirnya bisa terwujud.</p>
  						<p>Selamat berjumpa di SD ISLAM ASSA'ADAH Cipinang Jakarta Timur melalui media website ini,
  							semoga
  							melalui media ini kebutuhan dan layanan komunikasi dapat memenuhi harapan masyarakat dan
  							pihak-pihak yang berkepentingan.</p>
  						<p>Tujuan utama website ini adalah sebagai media informasi kepada masyarakat luas tentang
  							keberadaan
  							sekolah, sehingga sekolah menjadi lebih dikenal dan siap menerima masukan dan kritik
  							membangun dari
  							pihak luar demi terwujudnya visi - misi sekolah.</p>
  						<p>Kami berharap website ini bisa memberikan manfaat yang seluas-luasnya kepada keluarga besar
  							SD
  							ISLAM ASSA'ADAH CIPINANG JAKARTA TIMUR, dan dunia pendidikan, serta masyarakat luas pada
  							umumnya.</p>
  						<p>Akhir kata, tak lupa saya ucapkan terima kasih kepada pengelola web yang telah bekerja
  							keras
  							demi
  							terwujudnya web ini, serta seluruh guru, karyawan dan siswa SD ISLAM ASSA'ADAH sehingga
  							website
  							sekolah ini menjadi lebih berguna dan bermanfaat</p>
  						<p>Kepala Sekolah</p>
  						<p>Sri Dewi Retno Wulan, S.Pd</p>
  					</div>
  				</div>
  			</div>

  		</div>
  	</section><!-- End About Section -->

  	<!-- ======= About Section ======= -->
  	<section id="about" class="about">
  		<div class="container">
  			<center>
  				<div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
  					<div class="carousel-indicators">
  						<button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active"
  							aria-current="true" aria-label="Slide 1"></button>
  						<button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1"
  							aria-label="Slide 2"></button>
  					</div>
  					<div class="carousel-inner">
  						<div class="carousel-item active">
  							<img src="<?php echo base_url('assets/image/content/slide1.jpeg') ?>" alt="">
  							<div class="container">
  								<div class="carousel-caption text-start">
  									<h3 style="color: black;">Sarana pendidikan Sekolah Dasar Islam Assa'Adah</h3>
  								</div>
  							</div>
  						</div>
  						<div class="carousel-item">
  							<img src="<?php echo base_url('assets/image/content/slide2.jpeg') ?>" alt="">
  							<div class="container">
  								<div class="carousel-caption">

  								</div>
  							</div>
  						</div>

  					</div>
  					<button class="carousel-control-prev" type="button" data-bs-target="#myCarousel"
  						data-bs-slide="prev">
  						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
  						<span class="visually-hidden">Previous</span>
  					</button>
  					<button class="carousel-control-next" type="button" data-bs-target="#myCarousel"
  						data-bs-slide="next">
  						<span class="carousel-control-next-icon" aria-hidden="true"></span>
  						<span class="visually-hidden">Next</span>
  					</button>
  				</div>
  			</center>
  		</div>
  	</section><!-- End About Section -->

  	<section id="kegiatan" class="portfolio">
  		<div class="container">

  			<div class="section-title" data-aos="fade-up">
  				<h2>Gallery Sekolah Dasar Islam Assa'adah</h2>
  				<p>Berikut adalah beberapa foto kegiatan para siswa di Sekolah Dasar Islam Assa'Adah.</p>
  			</div>

  			<div class="row">
  				<div class="col-lg-12 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
  					<ul id="portfolio-flters">
  						<li data-filter="*" class="filter-active">All</li>
  					</ul>
  				</div>
  			</div>

  			<div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

  				<div class="col-lg-4 col-md-6 portfolio-item filter-app">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan.jpeg') ?>" width="356px" alt="">
  						<div class="portfolio-info">
  							<h4>Kegiatan Belajar</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox" title="Kegiatan Belajar"><i
  									class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-web">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan2.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Kegiatan Acara Buka Bersama</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan2.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox"
  								title="Kegiatan Acara Buka Bersama"><i class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-app">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan3.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Kegiatan Lomba</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan3.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox" title="Kegiatan Lomba"><i
  									class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-card">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan4.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Kegiatan Lomba</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan4.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox" title="Kegiatan Lomba"><i
  									class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-web">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan5.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Kegiatan Buka Bersama</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan5.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox"
  								title="Kegiatan Buka Bersama"><i class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-app">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan6.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Kegiatan Buka Bersama</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan6.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox"
  								title="Kegiatan Buka Bersama"><i class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-card">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan7.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Foto Guru</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan7.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox" title="Foto Guru"><i
  									class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  				<div class="col-lg-4 col-md-6 portfolio-item filter-card">
  					<div class="portfolio-wrap">
  						<img src="<?php echo base_url('assets/image/kegiatan/kegiatan8.jpeg') ?>" class="img-fluid" width="356px"
  							alt="">
  						<div class="portfolio-info">
  							<h4>Foto Guru</h4>
  						</div>
  						<div class="portfolio-links">
  							<a href="<?php echo base_url('assets/image/kegiatan/kegiatan8.jpeg') ?>"
  								data-gallery="portfolioGallery" class="portfolio-lightbox" title="Foto Guru"><i
  									class="bx bx-plus"></i></a>

  						</div>
  					</div>
  				</div>

  			</div>

  		</div>
  	</section><!-- End Portfolio Section -->


  	<!-- ======= F.A.Q Section ======= -->
  	<section id="faq" class="faq">
  		<div class="container">

  			<div class="section-title" data-aos="fade-up">
  				<h2>Panduan Pendaftaran PPDB ONLINE di Sekolah Dasar Islam ASSA'ADAH</h2>
  			</div>

  			<ul class="faq-list">

  				<li>
  					<div data-bs-toggle="collapse" class="collapsed question" href="#faq1">Penerimaan Peserta Didik
  						Baru (PPDB) dilaksanakan dengan ketentuan sebagai berikut:
  						<i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq1" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							Dinas Pendidikan dan sekolah diminta menyiapkan mekanisme PPDB
  							yang mengikuti protokol kesehatan untuk mencegah penyebaran Covid-19,
  							termasuk mencegah berkumpulnya siswa dan orangtua secara fisik di sekolah
  						</p>
  					</div>
  				</li>

  				<li>
  					<div data-bs-toggle="collapse" href="#faq2" class="collapsed question"> PPDB pada Jalur Prestasi
  						dilaksanakan berdasarkan:
  						<i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq2" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							akumulasi nilai rapor ditentukan berdasarkan nilai lima semester terakhir<br>dan/atau
  							prestasi akademik dan non-akademik di luar rapor sekolah
  						</p>
  					</div>
  				</li>

  				<li>
  					<div data-bs-toggle="collapse" href="#faq3" class="collapsed question">Pusat data dan informasi<i
  							class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq3" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							Pusat Data dan Informasi (Pusdatin) Kementerian Pendidikan dan
  							Kebudayaan menyediakan bantuan teknis bagi daerah yang memerlukan mekanisme PPDB daring.
  						</p>
  					</div>
  				</li>

  				<li>
  					<div data-bs-toggle="collapse" href="#faq4" class="collapsed question">Orangtua harus melakukan
  						register
  						dan melakukan
  						login untuk mendaftarkan anak nya.<i class="bi bi-chevron-down icon-show"></i><i
  							class="bi bi-chevron-up icon-close"></i></div>
  					<div id="faq4" class="collapse" data-bs-parent=".faq-list">
  						<p>
  							Setelah melakukan register, dan melakukan login orang tua akan mengisi form
  							untuk mendaftarkan anaknya sekolah.
  						</p>
  					</div>
  				</li>

  			</ul>

  		</div>
  	</section><!-- End Frequently Asked Questions Section -->

  	<!-- ======= Contact Section ======= -->
  	<section id="login" class="contact section-bg">
  		<div class="container">

  			<div class="section-title" data-aos="fade-up">
  				<h2>Login</h2>
  			</div>

  			<div class="row">
  				<div class="col-md-8" data-aos="fade-left">
  					<form action="<?php echo base_url('AuthController/verivLogin') ?>" method="post">
  						<!-- <div class="row"> -->
  						<div class="form-group mt-3">
  							<input type="text" class="form-control" name="username" required placeholder="username.....">
  						</div>
  						<div class="form-group mt-3 mb-2">
  							<input type="password" class="form-control" name="password" required placeholder="password...">
  						</div>
  						<button class="btn btn-primary btn-lg" type="submit">LOGIN</button>
  					</form>
  				</div>

				  <div class="col-md-4">
					  <center>
						  <img src="<?php echo base_url('assets/image/content/logo.jpeg') ?>" alt="" width="300px">
					  </center>
				  </div>
  			</div>

  		</div>
  	</section><!-- End Contact Section -->


  </main><!-- End #main -->
