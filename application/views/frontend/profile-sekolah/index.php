  	<!-- ======= About Section ======= -->
  	<section id="visi-misi" class="about">
  		<div class="container">

  			<div class="row">
  				<div class="col-lg-6 d-flex flex-column justify-contents-center" data-aos="fade-left">
  					<div class="content pt-4 pt-lg-0">
  						<h3>Visi dan Misi</h3>
  						<p>TERWUJUDNYA PEMBENTUKAN GENERASI ISLAM
  							BERKUALITAS, BERIMAN, BERTAOWA, CERDAS,
  							TERAMPIL, MANDIRI, BERBUDAYA SERTA
  							BERWAWASAN LINGKUNGAN</p>
  						<p>
  							Misi Dasar Islam Assa'adah Cipinang:<br>
  							MEMBANTU PEMERINTAH DALAM MEMBENTUK
  							BANGSA YANG BERPENDIDIKDN DAN BERMORAL dengan upaya:<br>
  							<ul>
  								<li>Menanamkan keyakinan akitah melalui pengamalan
  									ajaran agama</li>
  								<li>Mengoptimalkan proses pembelajaran dan bimbingan.</li>
  								<li>Mengembangkan pengetahuan di bidang ilmu dan
  									teknologi , bahasa, olahraga dan seni” budaya sesuai
  									dengan bakat, minat dan potensi peserta didik.</li>
  								<li>Membina kemandirian peserta didik melalui kegiatan
  									pembiasaan dan pengembangan diri yang terencana dan perduli
  									terhadap kelestarian lingkungan hidup</li>
  								<li>Menjalin kerjasama bidang pendidikan yang Harmonis
  									antara Warga sekolah, Pakar pendidikan, dan kerjasama
  									dengan instansi lintas sektoral untuk menciptakan
  									lingkungan sekolah yang asri,aman,nyaman, hijau dan
  									kondusif</li>
  								<li>Membiasakan Pemanfaatan Teknologi Informasi dalam Kegiatan Belajar Mengajar
  									dengan Berpedoman Pada Aspek Ramah Lingkungan.
  								</li>
  							</ul>
  						</p>
  					</div>
  				</div>
  			</div>

  		</div>
  	</section><!-- End About Section -->


  	<!-- ======= About Section ======= -->
  	<section id="tujuan" class="about">
  		<div class="container">

  			<div class="row">
  				<div class="col-lg-6" data-aos="zoom-in">
  					<!-- <img src="<?php echo base_url('assets/image/profile/kepalasekolah.jpeg') ?>" class="img-fluid"
  						alt=""> -->
  				</div>
  				<div class="col-lg-6 d-flex flex-column justify-contents-center" data-aos="fade-left">
  					<div class="content pt-4 pt-lg-0">
  						<h3>Tujuan Pendidikan</h3>
  						<p> Tujuan pendidikan adalah meletakkan
							pengetahuan, kepribadian, akhlak mulia, serta
							kecerdasan,
							keterampilan untuk hidup mandiri dan mengikuti pendidikar
							lebih lanjut</p>
						<p>Mengacu pada visi dan misi sekolah, serta tujuan umum
							pendidikan dasar, tujuan sekolah dalam mengembangkan
							pendidikan ini adalah sebagai berikut ini</p>

						<p>
							<ul>
								<li>Terbaik dalam pengembangan budaya sekolah yang religius melalui
									pembiasaan dan kegiatan keagamaan.
								</li>
								<li>Terbaik dalam pelaksanakan pembelajaran dengan pendekatan tematik
									integratif dan pendekatan Scientific melalui pembelajaran Pakem.
								</li>
								<li>Terbaik dalam kegiatan pembiasaan dan pengembangan
									diri yang terencana dan peduli terhadap kelestarian
									lingkungan hidup</li>
								<li>Mengembangkan berbagai pendekatan dan metode dalam
									proses belajar mengajar di kelas berbasis pendidikan
									budaya dan karakter bangsa.</li>
								<li>Menyelenggarakan berbagai kegiatan sosial yang menjadi
									bagian dari pendidikan budaya dan karakter bangsa.</li>
								<li>Menjalin kerja sama dengan lembaga pendidikan baik di
									dalam negeri maupun luar negeri.</li>
								<li>Memanfaatkan dan memelihara fasilitas sekolah untuk
									sebesar-besarnya mendukung proses belajar mengajar
									berbasis TIK.</li>
							</ul>
						</p>
  					</div>
  				</div>
  			</div>

  		</div>
  	</section><!-- End About Section -->


        	<!-- ======= About Section ======= -->
  	<section id="struktur" class="about">
  		<div class="container">

  			<div class="row">
  				<div class="col-lg-6" data-aos="zoom-in">
				  <div class="content pt-4 pt-lg-0">
  						<h3>Struktur Organisasi</h3>
  					<img src="<?php echo base_url('assets/image/struktur_organisasi.jpeg') ?>" class="img-fluid"
  						alt="">
				  </div>
  				</div>
  			</div>

  		</div>
  	</section><!-- End About Section -->
