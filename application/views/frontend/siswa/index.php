<section id="hero">

	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
				data-aos="fade-up">
				<div>
					<h4>DIREKTORI SISWA</h4>
					<table class="table table-striped table-bordered" id="table" width="100%">
				<thead>
					<tr>
						<th>NIS</th>
						<th>NAMA SISWA</th>
						<th>NAMA WALI KELAS</th>
						<th>KELAS</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($siswa as $childs) :?>
					<tr>
						<td><?php echo $childs->nis ?></td>
						<td><?php echo $childs->nama_lengkap ?></td>
						<td><?php echo $childs->nama_guru ?></td>
						<td><?php echo $childs->nama_kelas ?></td>
					</tr>
					<?php endforeach ;?>
				</tbody>
			</table>
				</div>
			</div>
		</div>
	</div>

</section>

<br><br><br><br><br><br><br><br><br><br><br><br>
<section id="alumni">

	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
				data-aos="fade-up">
				<div>
					<h4>DATA ALUMNI</h4>
					<table class="table table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>NAMA SISWA</th>
								<th>TAHUN LULUS</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Amira Sahla Ardiansyah</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Aulia Rahma Syahidah</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Azzikrillah Nur Fadillah</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Bambang Aliyudin</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Dimas Prasetyo</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Erlangga Pratama</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>M. Ikhsan Rosyidin</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Karaya Nurdifa</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>Maimunatul Munawaroh</td>
								<td>2019</td>
							</tr>
							<tr>
								<td>M. Varel Syahki Ramadhan</td>
								<td>2019</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</section>


<script>
	$(document).ready(function () {
		$('#table').DataTable({

		});
	});

</script>



<script>
	$(document).ready(function () {
		$('#table').DataTable({

		});
	});

</script>
