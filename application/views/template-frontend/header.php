<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>WELCOME TO SDI ASSA'ADAH</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/sweetalert.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?php echo base_url('assets/js/sweetalert.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" />
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url('assets/frontend/css/bootstrap.min.css') ?>" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}

	</style>


	<!-- Custom styles for this template -->
	<link href="<?php echo base_url('assets/frontend/css/carousel.css') ?>" rel="stylesheet">
</head>


<header>
	<nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" href="#"><img src="<?php echo base_url('assets/image/logo.jpeg') ?>" height="50px" alt=""></a>
			<!-- <center> -->
			<ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
				<li><a href="<?php echo base_url('welcome') ?>" class="nav-link px-2 link-secondary">Home</a></li>
				<li><a href="<?php echo base_url('welcome/profile') ?>" class="nav-link px-2 link-dark">Profil Sekolah</a></li>
				<li>
					<div class="dropdown text-end">
						<a href="#" class="nav-link px-2 link-dark dropdown-toggle" id="dropdown"
							data-bs-toggle="dropdown">Guru</a>
						<ul class="dropdown-menu text-small" aria-labelledby="dropdown">
							<li><a class="dropdown-item" href="<?php echo base_url('welcome/guru') ?>">Directori Guru</a></li>
						</ul>
					</div>
				</li>
				<li>
					<div class="dropdown text-end">
						<a href="#" class="nav-link px-2 link-dark dropdown-toggle" id="dropdown1"
							data-bs-toggle="dropdown">Siswa</a>
						<ul class="dropdown-menu text-small" aria-labelledby="dropdown1">
							<li><a class="dropdown-item" href="<?php echo base_url('welcome/siswa') ?>">Directori Siswa</a></li>
						</ul>
					</div>
				</li>
				<li>
					<div class="dropdown text-end">
						<a href="#" class="nav-link px-2 link-dark dropdown-toggle" id="dropdown2"
							data-bs-toggle="dropdown">PPDB</a>
						<ul class="dropdown-menu text-small" aria-labelledby="dropdown2">
							<li><a class="dropdown-item" href="<?php echo base_url('Welcome/ppdb') ?>">Petunjuk PPDB</a></li>
							<li><a class="dropdown-item" href="<?php echo base_url('Welcome/register') ?>">Register</a></li>
						</ul>
					</div>
				</li>
			</ul>
			<!-- </center> -->
		</div>
		</div>
	</nav>
</header>

<body>
