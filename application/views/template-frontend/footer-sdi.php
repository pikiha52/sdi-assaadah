  <footer id="footer">
  	<div class="footer-top">
  		<div class="container">
  			<div class="row">

  				<div class="col-lg-9 col-md-6">
  					<div class="footer-info">
  						<h3>Sdi Assa'adah</h3>
  						<p>
  							Jl Madrasah No.14 RT004/RW0011, <br>
  							Kel.Cipinang, Kel.Pulogadung, <br>
  							Kota Jakarta Timur<br><br>
  							<strong>Phone:</strong> +62 21 282 23<br>
  							<strong>Email:</strong> sdi@assadah.com<br>
  						</p>
  					</div>
  				</div>
  				<!-- <div class="col-lg-3 col-md-6">
				  <img src="<?php echo base_url('assets/image/content/logo.jpeg') ?>" alt="">
				  </div> -->
  			</div>
  		</div>
  	</div>

  	<div class="container">
  		<div class="copyright">
  			&copy; Copyright <strong><span>Sdi Assa'adah</span></strong>.
  		</div>
  	</div>
  </footer>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
  		class="bi bi-arrow-up-short"></i></a>

  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/frontend/js/bootstrap.bundle.min.js') ?>" ></script>
  
  <script src="<?php echo base_url('assets/vendor/aos/aos.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/glightbox/js/glightbox.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/isotope-layout/isotope.pkgd.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/php-email-form/validate.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/swiper/swiper-bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/main.js') ?>"></script>

  <!-- Core plugin Datatables -->
  <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"> </script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>

  <!-- message sweetlert -->
  <script>
    		//sweetalert for success or error message
    		<?php if($this->session->flashdata('success') == 'message_success'):?>
    		    swal({
    		        type: "success",
    		        icon: "success",
    		        title: "Berhasil",
    		        text: "Selamat anda berhasil, silahkan lakukan login! ",
    		        timer: 1800,
    		        showConfirmButton: false,
    		        showCancelButton: false,
    		        buttons: false,
    		    });
    		    <?php elseif($this->session->flashdata('error') == 'message_error'):?>
    		        swal({
    		            type: "error",
    		            icon: "error",
    		            title: "Gagal!",
    		            text: " Maaf terjadi kesalahan!",
    		            timer: 1800,
    		            showConfirmButton: false,
    		            showCancelButton: false,
    		            buttons: false,
    		        });
    		    <?php endif; ?>
    		</script>
  </body>

  </html>
