<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title><?= $title ?></title>
	<meta content="" name="description">
	<meta content="" name="keywords">
	<link href="<?php echo base_url('assets/image/content/logo.jpeg') ?>" rel="icon">
	<link href="<?php echo base_url('assets/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">
	<link
		href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
		rel="stylesheet">
	<link href="<?php echo base_url('assets/vendor/aos/aos.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendor/bootstrap-icons/bootstrap-icons.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendor/boxicons/css/boxicons.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendor/glightbox/css/glightbox.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendor/swiper/swiper-bundle.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/sweetalert.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?php echo base_url('assets/js/sweetalert.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" />
	<link href="<?php echo base_url('assets/frontend/css/carousel.css') ?>" rel="stylesheet">
</head>

<body>


	<header id="header" class="fixed-top d-flex align-items-center">
		<div class="container d-flex align-items-center">

			<div class="logo me-auto">
			<h1><img src="<?php echo base_url('assets/image/content/logo.jpeg') ?>" alt="">
				<a class="px-1" href="<?php echo base_url('welcome') ?>">Sekolah Islam Assa'adah</a></h1>
			</div>

			<nav id="navbar" class="navbar order-last order-lg-0">
				<ul>
					<li><a class="nav-link scrollto" href="<?php echo base_url('welcome')?>#hero">Home</a></li>
					<li class="dropdown"><a href="<?php echo base_url('welcome/profile') ?>"><span>Profil Sekolah</span>
							<i class="bi bi-chevron-down"></i></a>
						<ul>
							<li><a class="nav-link scrollto"
									href="<?php echo base_url('welcome/profile')?>#visi-misi">Visi dan Misi</a></li>
							<li><a class="nav-link scrollto"
									href="<?php echo base_url('welcome/profile')?>#tujuan">Tujuan Pembelajaran</a></li>
							<li><a class="nav-link scrollto"
									href="<?php echo base_url('welcome/profile')?>#struktur">Struktur Organisasi</a>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a href="#"><span>Guru</span> <i class="bi bi-chevron-down"></i></a>
						<ul>
							<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/guru') ?>">Direktori
									Guru</a></li>
									<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/silabus') ?>#silabus">Silabus</a></li>
									<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/silabus') ?>#kalender">Kalender Akademik</a></li>
							<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/silabus') ?>#ajar">Materi Ajar</a></li>
							<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/silabus') ?>#uji">Materi Uji</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="#"><span>Siswa</span> <i class="bi bi-chevron-down"></i></a>
						<ul>
							<li><a class="nav-link" href="<?php echo base_url('welcome/siswa') ?>">Direktori Siswa</a>
							</li>
                            <li><a class="nav-link" href="<?php echo base_url('welcome/siswa') ?>#alumni">Data Alumni</a>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a href="#"><span>PPDB ONLINE</span> <i class="bi bi-chevron-down"></i></a>
						<ul>
							<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/ppdb') ?>#petunjuk">Petunjuk PPDB
									ONLINE</a></li>
							<li><a class="nav-link scrollto" href="<?php echo base_url('welcome/ppdb') ?>#register">Register</a></li>
						</ul>
					</li>
				</ul>
				<i class="bi bi-list mobile-nav-toggle"></i>
			</nav><!-- .navbar -->

		</div>
	</header><!-- End Header -->
