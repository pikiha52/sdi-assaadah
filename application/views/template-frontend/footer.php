
	<br><br>
	<!-- FOOTER -->
		<footer class="container">
			<p class="float-end"><a href="#" class="btn btn-primary"><i class="fa fa-angle-up"></i></a></p>
			<p>&copy; SD Islam Asa'adah</p>
		</footer>
	</main> 

	<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/frontend/js/bootstrap.bundle.min.js') ?>">
	</script>
            <!-- Core plugin Datatables -->
            <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"> </script>
            <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

		<script>
    		//sweetalert for success or error message
    		<?php if($this->session->flashdata('success') == 'message_success'):?>
    		    swal({
    		        type: "success",
    		        icon: "success",
    		        title: "Berhasil",
    		        text: "Selamat anda berhasil, silahkan lakukan login! ",
    		        timer: 1800,
    		        showConfirmButton: false,
    		        showCancelButton: false,
    		        buttons: false,
    		    });
    		    <?php elseif($this->session->flashdata('error') == 'message_error'):?>
    		        swal({
    		            type: "error",
    		            icon: "error",
    		            title: "Gagal!",
    		            text: " Maaf terjadi kesalahan!",
    		            timer: 1800,
    		            showConfirmButton: false,
    		            showCancelButton: false,
    		            buttons: false,
    		        });
    		    <?php endif; ?>
    		</script>

</body>

</html>
