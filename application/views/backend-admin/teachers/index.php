                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">All Teachers</h1>
                    </div>


                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">All Teacher in SDI Assa'adah</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>KODE GURU</th>
                                            <th>NAMA LENGKAP</th>
                                            <th>WALI KELAS/JABATAN</th>
                                            <th>TANGGAL LAHIR</th>
                                            <th>PENDIDIKAN</th>
                                            <th>TANGGAL DIBUAT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($teachers as $teach) :?>
                                        <tr>
                                            <td><?php echo $teach->code_teacher ?></td>
                                            <td><?php echo $teach->name ?></td>
                                            <td><?php echo $teach->nama_kelas ?></td>
                                            <td><?php echo $teach->date_birth ?></td>
                                            <td><?php echo $teach->education ?></td>
                                            <td><?php echo $teach->created_at ?></td>
                                        </tr>
                                     <?php endforeach ;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


                <script>
                    $(document).ready(function () {
                        $('#table').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'excel', 'pdf', 'print'
                            ]
                        });
                    });
                    $('#id-table').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": false,
                        "autoWidth": true,
                    });
                </script>