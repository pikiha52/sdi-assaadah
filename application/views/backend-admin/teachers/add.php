                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Add Teachers</h1>
                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Form add Teachers</h6>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="form-group col-md-12">
                                    <label for="firstname">Fullname</label>
                                    <input type="text" class="form-control" name="name">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="lastname">Date of Birth</label>
                                    <input type="date" class="form-control" name="date_birth">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="lastname">Education</label>
                                    <input type="text" class="form-control" name="education">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="code">Position</label>
                                    <input type="text" class="form-control" name="position">
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-secondary">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>