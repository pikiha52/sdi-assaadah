                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">DATA SISWA</h1>
                	</div>


                	<div class="card shadow mb-4">
                		<div class="card-body">
                			<div class="table-responsive">
                				<table class="table table-bordered table-hover" id="table-id" width="100%"
                					cellspacing="0">
                					<thead>
                						<tr>
                							<th>NIS</th>
                							<th>NAMA SISWA</th>
                							<th>KELAS</th>
                							<th>WALI KELAS</th>
                							<th>AKSI</th>
                						</tr>
                					</thead>
                					<tbody>
                						<?php foreach($students as $stud) :?>
                						<tr>
                							<td><?php echo $stud->nis ?></td>
                							<td><?php echo $stud->nama_lengkap ?></td>
                							<td><?php echo $stud->nama_kelas ?></td>
                							<td><?php echo $stud->nama_guru ?></td>
                							<td>
                								<a href="<?php echo base_url('admin/studentscontroller/update/'. $stud->id)?>" class="btn btn-info"><i
                										class="fa fa-pen"></i></a>
                							</td>
                						</tr>
                						<?php endforeach ;?>
                					</tbody>
                				</table>
                			</div>
                		</div>
                	</div>

                </div>


                <!-- <script>
                    $(document).ready(function () {
                        $('#table-id').DataTable({
                           
                        });
                    });
                    $('#id-table').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                    });
                </script> -->
