            <!-- Begin Page Content -->
            <div class="container-fluid">

            	<!-- Page Heading -->
            	<div class="d-sm-flex align-items-center justify-content-between mb-4">
            		<h1 class="h3 mb-0 text-gray-800">DATA PPDB ONLINE</h1>
            	</div>


            	<div class="card shadow mb-4">
            		<div class="card-body">
            			<div class="table-responsive">
            				<table class="table table-bordered table-hover" id="table-id" width="100%" cellspacing="0">
            					<thead>
            						<tr>
            							<th>NAMA</th>
            							<th>NAMA PANGGILAN</th>
            							<th>UMUR</th>
            							<th>JENIS KELAMIN</th>
            							<th>TEMPAT, TANGGAL LAHIR</th>
            							<th>NAMA AYAH</th>
            							<th>NAMA IBU</th>
            							<th>PEKERJAAN AYAH</th>
            							<th>PEKERJAAN IBU</th>
            							<th>PENGHASILAN</th>
            							<th>STATUS</th>
            							<th>BUKTI TRANSFER</th>
            							<th>TANGGAL DAFTAR</th>
            						</tr>
            					</thead>
            					<tbody>
            						<?php foreach($students as $stud) :?>
            						<tr>
            							<td><?php echo $stud->nama_lengkap ?> </td>
            							<td><?php echo $stud->nama_p ?></td>
            							<td><?php echo $stud->umur ?></td>
            							<td><?php echo $stud->jen_kel ?></td>
            							<td><?php echo $stud->tempat_lahir.','.$stud->tgl_lahir ?></td>
            							<td><?php echo $stud->nama_a ?></td>
            							<td><?php echo $stud->nama_i ?></td>
            							<td><?php echo $stud->pekerjaan_a ?></td>
            							<td><?php echo $stud->pekerjaan_i ?></td>
            							<td>Rp <?php echo $stud->penghasilan ?></td>
            							<td style="text-align: center;"><a data-target="#update<?php echo $stud->id ?>"
            									data-toggle="modal" class="btn btn-primary"
            									title="klik untuk ubah status"><?php echo $stud->status ?></a></td>
            							<td><a href="#" data-toggle="modal" data-target="#imagemodal<?php echo $stud->id ?>"><img
            										src="<?php echo base_url('assets/document/'. $stud->bukti_tf) ?>"
            										style="width:100%;max-width:300px" alt=""></a></td>
            							<td><?php echo $stud->created_at ?></td>
            						</tr>
            						<?php endforeach ;?>
            					</tbody>
            				</table>
            			</div>
            		</div>
            	</div>
            </div>

            <?php $no = 0;
				foreach ($students as $stud) : $no++; ?>
            <!-- Update Modal -->
            <div class="modal fade" id="update<?php echo $stud->id ?>" tabindex="-1" role="dialog"
            	aria-labelledby="exampleModalLabel" aria-hidden="true">
            	<div class="modal-dialog" role="document">
            		<div class="modal-content">
            			<form action="<?php echo base_url('admin/StudentsController/updateStatus') ?>"
            				enctype="multipart/form-data" method="POST">
            				<div class="modal-header">
            					<h5 class="modal-title" id="exampleModalLabel">UBAH STATUS</h5>
            					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            						<span aria-hidden="true">&times;</span>
            					</button>
            				</div>
            				<div class="modal-body">
            					<div class="form-group col-md-12">
            						<label for="status">STATUS</label>
            						<input type="hidden" class="form-control" name="id" value="<?php echo $stud->id ?>"
            							readonly>
            						<select name="status" id="status" class="form-control">
            							<option value="<?php echo $stud->status ?>"><?php echo $stud->status ?></option>
            							<option value="diterima">diterima</option>
            						</select>
            					</div>
            				</div>
            				<div class="modal-footer">
            					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            					<button type="submit" class="btn btn-primary">Save</button>
            				</div>
            			</form>
            		</div>
            	</div>
            </div>
            <?php endforeach ;?>


            <?php $no = 0;
				foreach ($students as $stud) : $no++; ?>
            <!-- MODAL BUKTI -->
            <div class="modal fade" id="imagemodal<?php echo $stud->id ?>" tabindex="-1" role="dialog"
            	aria-labelledby="exampleModalLabel" aria-hidden="true">
            	<div class="modal-dialog" role="document">
            		<div class="modal-content">
            			<div class="modal-header">
            				<h5 class="modal-title" id="exampleModalLabel">BUKTI TRANSFER <?php echo $stud->nama_lengkap ?></h5>
            				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            					<span aria-hidden="true">&times;</span>
            				</button>
            			</div>
            			<div class="modal-body">
            				<figure class="figure">
            					<img src="<?php echo base_url('assets/document/'. $stud->bukti_tf) ?>" class="figure-img img-fluid rounded"
            						alt="A generic square placeholder image with rounded corners in a figure.">
            				</figure>
            			</div>
            		</div>
            	</div>
            </div>
            <?php endforeach ;?>


            <script>
            	$(document).ready(function () {
            		$('#table-id').DataTable({

            		});
            	});
            	$('#id-table').DataTable({
            		"paging": true,
            		"lengthChange": false,
            		"searching": false,
            		"ordering": true,
            		"info": false,
            		"autoWidth": true,
            	});

            </script>
