                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">TAMBAH SISWA</h1>
                	</div>

                	<div class="card shadow mb-4">
                		<div class="card-body">
                			<form action="<?php echo base_url('admin/studentscontroller/updatestore') ?>" method="POST">
                				<?php foreach($students as $stud) :?>

                				<input type="hidden" class="form-control" name="id" value="<?php echo $stud->id ?>">
                				<div class="form-group col-md-12">
                					<label for="lastname">NAMA LENGKAP</label>
                					<input type="text" class="form-control" readonly name="nama_lengkap" value="<?php echo $stud->nama_lengkap ?>">
                				</div>

                				<div class="form-group col-md-12">
                					<label for="lastname">NOMOR INDUK SEKOLAH</label>
                					<input type="text" class="form-control" readonly name="nis" value="<?php echo $stud->nis ?>">
                				</div>

                				<div class="form-group col-md-12">
                					<label for="firstname">KELAS</label>
                					<select name="kelas_id" id="teacher_id" class="form-control">
                						<?php foreach($kelas as $kel):?>
                						<option value="<?= $kel->id?>" <?php if ($kel->id == $stud->kelas_id) : ?>
                							selected<?php endif; ?>>
                							<?= $kel->name?>
                						</option>
                						<?php endforeach; ?>
                					</select>
                				</div>
                				<?php endforeach ;?>

                				<div class="modal-footer">
                					<button type="submit" class="btn btn-primary">SIMPAN</button>
                				</div>
                			</form>
                		</div>
                	</div>

                </div>
