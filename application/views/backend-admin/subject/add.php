                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">TAMBAH MATA PELAJARAN</h1>
                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <form action="<?php echo base_url('admin/SubjectController/store') ?>" method="POST">
                                <div class="form-group col-md-12">
                                    <label for="firstname">KELAS</label>
                                    <select name="kelas_id" id="teacher_id" class="form-control">
                                    <option value="">PILIH KELAS</option>
                                    <?php foreach($kelas as $teach) :?>
                                    <option value="<?php echo $teach->id ?>"><?php echo $teach->name ?></option>
                                    <?php endforeach ;?>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="lastname">NAMA MATA PELAJARAN</label>
                                    <input type="text" class="form-control" name="name">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="lastname">NILAI MINIMAL</label>
                                    <input type="text" class="form-control" name="point_kkm">
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>