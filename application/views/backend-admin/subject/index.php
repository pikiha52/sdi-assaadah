                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">DATA MATA PELAJARAN</h1>
                    </div>


                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>NAMA GURU</th>
                                            <th>KELAS</th>
                                            <th>MATA PELAJARAN</th>
                                            <th>NILAI MINIMAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($subject as $sub) :?>
                                        <tr>
                                            <td><?php echo $sub->nama_guru ?></td>
                                            <td><?php echo $sub->nama_kelas ?></td>
                                            <td><?php echo $sub->name ?></td>
                                            <td><?php echo $sub->point_kkm ?></td>
                                        </tr>
                                        <?php endforeach ;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


                <script>
                    $(document).ready(function () {
                        $('#table').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'excel', 'pdf', 'print'
                            ]
                        });
                    });
                    $('#id-table').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": false,
                        "autoWidth": true,
                    });
                </script>