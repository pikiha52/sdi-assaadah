                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">TAMBAH USER</h1>
                	</div>

                	<div class="card shadow mb-4">
                		<div class="card-body">
                			<form action="<?php echo base_url('admin/UsersController/store') ?>" method="POST"
                				enctype="multipart/form-data">

                				<div class="form-group col-md-12">
                					<label for="username">USERNAME</label>
                					<input type="text" class="form-control" name="username">
                				</div>

                				<div class="form-group col-md-12">
                					<label for="lastname">NAMA LENGKAP</label>
                					<input type="text" class="form-control" name="name">
                				</div>


                				<div class="form-group col-md-12">
                					<label for="lastname">NOMOR HANDPHONE (WA)</label>
                					<input type="number" class="form-control" name="phone">
                				</div>

                				<div class="form-group col-md-12">
                					<label for="lastname">ALAMAT</label>
                					<input type="text" class="form-control" name="address">
                				</div>

                				<div class="form-group col-md-12">
                					<label for="lastname">PASSWORD</label>
                					<input type="password" class="form-control" name="password">
                				</div>

                				<div class="form-group col-md-12">
                					<label for="code">STATUS</label>
                					<select name="status" class="form-control" id="status">
                						<option value="admin">ADMIN</option>
                						<option value="teacher">GURU</option>
                						<option value="wali">WALI</option>
                					</select>
                				</div>

                				<div class="form-group col-md-12">
                					<p class="text-sm-left">jika status guru wajib mengisi data tambahan ditambah data
                						guru.</p>
                					<a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1"
                						role="button" aria-expanded="false" aria-controls="multiCollapseExample1">TAMBAH
                						DATA GURU</a>
                				</div>


                				<div class="collapse multi-collapse" id="multiCollapseExample1">

                					<div class="form-group col-md-12">
                						<label for="lastname">GAMBAR</label>
                						<input type="file" class="form-control" name="image">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="lastname">TEMPAT LAHIR</label>
                						<input type="text" class="form-control" name="place_ofbirth">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="lastname">TANGGAL LAHIR</label>
                						<input type="date" class="form-control" name="date_birth">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="lastname">PENDIDIKAN</label>
                						<input type="text" class="form-control" name="education">
                					</div>
                				</div>

                				<div class="modal-footer">
                					<button type="submit" class="btn btn-primary">SIMPAN</button>
                				</div>
                			</form>
                		</div>
                	</div>

                </div>
