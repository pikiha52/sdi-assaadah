                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">DATA USER</h1>
                	</div>


                	<div class="card shadow mb-4">
                		<div class="card-body">
                			<div class="table-responsive">
                				<table class="table table-bordered" id="table" width="100%" cellspacing="0">
                					<thead>
                						<tr>
                							<th>GAMBAR</th>
                							<th>USERNAME</th>
                							<th>NAMA LENGKAP</th>
                							<th>NOMOR HANDPHONE</th>
                							<th>ALAMAT</th>
                							<th>STATUS</th>
                							<th>TANGGAL DAFTAR</th>
                							<th>AKSI</th>
                						</tr>
                					</thead>
                					<tbody>
                						<?php foreach($users as $user) :?>
                						<tr>
                							<td><img src="<?php echo base_url('assets/image/profile/'.$user->image) ?>" height="100" alt="">
                							</td>
                							<td><?php echo $user->username ?></td>
                							<td><?php echo $user->name ?></td>
                							<td><?php echo $user->phone ?></td>
                							<td><?php echo $user->address ?></td>
                							<td><a href="<?php echo base_url('admin/UsersController/updateStatus') ?>"
                									class="btn btn-info"><?php echo $user->status ?></a></td>
                							<td><?php echo $user->created_at ?></td>
                							<td>
                								<a href="#" data-toggle="modal"
                									data-target="#ubah<?php echo $user->id?>"
                									class="btn btn-circle btn-info"><i class="fa fa-pen"></i></a>
                							</td>
                						</tr>
                						<?php endforeach ;?>
                					</tbody>
                				</table>
                			</div>
                		</div>
                	</div>

                </div>

                <?php $no = 0;
        foreach ($users as $user) : $no++; ?>
                <!-- Modal -->
                <form action="<?php echo base_url('admin/UsersController/update') ?>" method="POST"
                	enctype="multipart/form-data">
                	<div class="modal fade" id="ubah<?php echo $user->id ?>" tabindex="-1" role="dialog"
                		aria-labelledby="exampleModalLabel" aria-hidden="true">
                		<div class="modal-dialog" role="document">
                			<div class="modal-content">
                				<div class="modal-header">
                					<h5 class="modal-title" id="exampleModalLabel">Update Users</h5>
                					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                						<span aria-hidden="true">&times;</span>
                					</button>
                				</div>
                				<div class="modal-body">
                					<div class="form-group col-md-12">
                						<label for="username">Image</label>
                						<input type="hidden" name="id" id="id" value="<?php echo $user->id ?>">
                						<input type="file" class="form-control" name="image"
                							value="<?php echo $user->image ?>">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="username">Username</label>
                						<input type="text" class="form-control" name="username"
                							value="<?php echo $user->username ?>">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="lastname">Full Name</label>
                						<input type="text" class="form-control" name="name"
                							value="<?php echo $user->name ?>">
                					</div>


                					<div class="form-group col-md-12">
                						<label for="lastname">Number Phone</label>
                						<input type="number" class="form-control" name="phone"
                							value="<?php echo $user->phone ?>">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="lastname">Address</label>
                						<input type="text" class="form-control" name="address"
                							value="<?php echo $user->address ?>">
                					</div>

                					<div class="form-group col-md-12">
                						<label for="code">Status</label>
                						<select name="status" class="form-control" id="status">
                							<option value="<?php echo $user->status ?>"><?php echo $user->status ?>
                							</option>
                							<option value="admin">Admin</option>
                							<option value="teacher">Teachers</option>
                							<option value="wali">Wali</option>
                						</select>
                					</div>
                				</div>
                				<div class="modal-footer">
                					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                					<button type="submit" class="btn btn-primary">Save changes</button>
                				</div>
                			</div>
                		</div>
                	</div>
                </form>
                <?php endforeach ?>


                <script>
                	$(document).ready(function () {
                		$('#table').DataTable({
                			// dom: 'Bfrtip',
                			// buttons: [
                			// 	'excel', 'pdf', 'print'
                			// ]
                		});
                	});
                	$('#id-table').DataTable({
                		"paging": true,
                		"lengthChange": false,
                		"searching": false,
                		"ordering": true,
                		"info": false,
                		"autoWidth": true,
                	});

                </script>
