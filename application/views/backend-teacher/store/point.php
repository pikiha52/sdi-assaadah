                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                	</div>

                	<!-- Content Row -->
                	<div class="row">
                		<div class="col-md-12">
                			<div class="card shadow mb-">
                				<div class="card-header py-3">
                					<h6 class="m-0 font-weight-bold text-primary">Absent Students</h6>
                				</div>

                				<div class="card-body">
                					<form action="<?php echo base_url('teacher/StudentsController/storePoint') ?>"
                						method="POST">
                						<div class="table-responsive">
                							<table class=" table table-bordered table-hover" id="table-id"
                								style="font-size:13px;">
                								<thead>
                									<th>NAMA SISWA</th>
                									<th>MATA PELAJARAN</th>
                									<th>NILAI</th>
													<th>TANGGAL</th>
                								</thead>
                								<tbody>
                									<?php foreach($students as $stud) :?>
                									<tr>
                										<td><input type="checkbox" name="student_id[]" id="student_id"
                												value="<?php echo $stud->student_id ?>"> <?php echo $stud->nama_lengkap ?>
                										</td>
                										<td>
															<select name="matpel[]" id="matpel">
																<option value="">Select Subjects</option>
																<?php foreach($subject as $sub) :?>
																<option value="<?php echo $sub->id ?>"><?php echo $sub->name ?></option>
																<?php endforeach ;?>
															</select>
                										</td>
														<td><input type="number" name="point[]" id=""></td>
                										<td><input type="hidden" name="date_point[]"
                												value="<?= $today ?>"><?= $today ?></td>
                									</tr>
                									<?php endforeach ;?>
                								</tbody>
                							</table>
                						</div>
                						<button type="submit" class="btn btn-primary">Submit</button>
                					</form>
                				</div>
                			</div>
                		</div>

                	</div>
                	<!-- End of Main Content -->
                </div>
