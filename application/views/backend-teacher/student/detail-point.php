                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">Details Point</h1>
                	</div>

                	<!-- Content Row -->
                	<div class="row">
                		<div class="col-md-12">
                			<div class="card shadow mb-">
                				<div class="card-header py-3">
                					<h6 class="m-0 font-weight-bold text-primary">DETAIL NILA SISWA -
                						<?php echo $studName->nama_lengkap ?></h6>
                				</div>

                				<div class="card-body">
								<form class="form-inline" action="<?php echo base_url('teacher/DetailStudentsPointController/bydate/'. $studName->id ) ?>" method="POST">
                						<div class="form-group mx-sm-3 mb-2">
                							<input type="date" class="form-control" id="inputPassword2"
                								placeholder="" name="date">
                						</div>
                						<button type="submit" class="btn btn-info mb-2"><i class="fa fa-search"></i></button>
                					</form>
                					<div class="table-responsive">
                						<table class=" table table-bordered table-hover" id="table-id"
                							style="font-size:13px;">
                							<thead>
                								<th>MATA PELAJARAN</th>
                								<th>KELAS</th>
                								<th>NILAI</th>
												<th>NILAI MINIMAL</th>
                								<th>TANGGAL</th>
                								<th>AKSI</th>
                							</thead>
                							<tbody>
												<?php if($students == NULL) :?>
													<div class="alert alert-info d-flex align-items-center" role="alert">
                									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                										fill="currentColor"
                										class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2"
                										viewBox="0 0 16 16" role="img" aria-label="Warning:">
                										<path
                											d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                									</svg>
                									<div>
                										DATA NILAi KOSONG
                									</div>
                								</div>
												<?php else :?>
                								<?php foreach($students as $stud) :?>
                								<tr>
                									<td><?php echo $stud->name ?></td>
                									<td><?php echo $stud->nama_kelas ?></td>
                									<td><?php echo $stud->point ?></td>
													<td><?php echo $stud->point_kkm ?></td>
                									<td><?php echo $stud->date_point ?></td>
                									<td>
													<a href="" data-toggle="modal" data-target="#edit<?php echo $stud->id ?>"
                											class="btn btn-info"><i class="fa fa-pen"></i></a>
													<a href="<?php echo base_url('teacher/DetailStudentsPointController/delete/'. $stud->id ) ?>" onclick="return confirm('Apa kamu yakin ingin menghapus data ini?')"
                											class="btn btn-danger"><i class="fa fa-trash"></i></a>
													</td>
                								</tr>
                								<?php endforeach ;?>
												<?php endif ;?>
                							</tbody>
                						</table>
                					</div>
                				</div>
                			</div>
                		</div>

                	</div>
                	<!-- End of Main Content -->
                </div>



                <?php $no = 0;
				foreach ($students as $stud) : $no++; ?>
                <div class="modal fade" id="edit<?php echo $stud->id?>" tabindex="-1" role="dialog"
                	aria-labelledby="exampleModalLabel" aria-hidden="true">
                	<div class="modal-dialog" role="document">
                		<div class="modal-content">
                			<form action="<?php echo base_url(). 'teacher/DetailStudentsPointController/update'; ?>"
                				enctype="multipart/form-data" method="post">
                				<div class="modal-header">
                					<h5 class="modal-title" id="exampleModalLabel">Edit Point</h5>
                					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                						<span aria-hidden="true">&times;</span>
                					</button>
                				</div>
                				<div class="modal-body">
      
                					<div class="form-group">
                						<label>Subjects</label>
                						<input class="form-control" name="point_id" type="hidden"
                							value="<?php echo $stud->id ?>">
											<input class="form-control" name="student_id" type="hidden"
                							value="<?php echo $stud->student_id ?>">
										<select name="matpel" id="matpel" class="form-control">
										<option value="<?php echo $stud->matpel_id ?>"><?php echo $stud->name ?></option>
										<?php foreach($subject as $sub) :?>
											<option value="<?php echo $sub->id ?>"><?php echo $sub->name ?></option>
										<?php endforeach ;?>
										</select>
                					</div>

									<div class="form-group">
                						<label>Point</label>
                						<input class="form-control" name="point" type="text"
                							value="<?php echo $stud->point ?>">
                					</div>
                				
                				</div>

                				<div class="modal-footer">
                					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                					<button type="submit" value="upload" class="btn btn-primary">Save</button>
                				</div>
                			</form>
                		</div>
                	</div>
                </div>


                <?php endforeach;?>
