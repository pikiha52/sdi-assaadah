                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">Absent</h1>
                	</div>

                	<!-- Content Row -->
                	<div class="row">
                		<div class="col-md-12">
                			<div class="card shadow mb-">
                				<!-- <div class="card-header py-3">
                					<h6 class="m-0 font-weight-bold text-primary">List Students</h6>
                				</div> -->

                				<div class="card-body">
								<div class="table-responsive">
							<table class=" table table-bordered table-hover" id="table-id" style="font-size:13px;">
								<thead>
									<th>NAMA SISWA</th>
									<th>AKSI</th>
								</thead>
								<tbody>
								<?php foreach($students as $stud) :?>
								<tr>
								<td><?php echo $stud->nama_lengkap ?></td>
								<td><a href="<?php echo base_url('teacher/DetailStudentsAbsentController/showAbsent/'. $stud->student_id) ?>" class="btn btn-info sm" ><i class="fa fa-search" ></i></a></td>
								</tr>
								<?php endforeach ;?>
								</tbody>
							</table>
						</div>
                				</div>
                			</div>
                		</div>

                	</div>
                	<!-- End of Main Content -->
				</div>

