                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">DATA ANAK</h1>
                	</div>


                	<div class="card shadow mb-4">
                		<div class="card-header py-3">
                			<h6 class="m-0 font-weight-bold text-primary">SDI ASSA'ADDAH</h6>
                		</div>
                		<div class="card-body">
                			<div class="table-responsive">
                				<table class="table table-bordered" id="table" width="100%" cellspacing="0">
                					<thead>
                						<tr>
                							<th>NAMA</th>
                							<th>WALI KELAS</th>
                							<th>KELAS</th>
                							<th>ABSEN</th>
                							<th>NILAI</th>
                						</tr>
                					</thead>
                					<tbody>

										<tr>
											<td><?php echo $childs->nama_lengkap ?></td>
											<td><?php echo $childs->nama_guru ?></td>
											<td><?php echo $childs->nama_kelas ?></td>
											<td><a href="<?php echo base_url('wali/ChildController/showAbsent/'. $childs->student_id ) ?>" 
												class="btn btn-info" title="Lihat detail absen anak?"><i class="fa fa-search" ></i></a></td>
											<td><a href="<?php echo base_url('wali/ChildController/showPoint/'. $childs->student_id ) ?>" 
												class="btn btn-info" title="Lihat detail nilai anak?"><i class="fa fa-search" ></i></a></td>
										</tr>

                					</tbody>
                				</table>
                			</div>
                		</div>
                	</div>

                </div>


                <!-- <script>
                	$(document).ready(function () {
                		$('#table').DataTable({
                			dom: 'Bfrtip',
                			buttons: [
                				'excel', 'pdf', 'print'
                			]
                		});
                	});
                	$('#id-table').DataTable({
                		"paging": true,
                		"lengthChange": false,
                		"searching": false,
                		"ordering": true,
                		"info": false,
                		"autoWidth": true,
                	});

                </script> -->
