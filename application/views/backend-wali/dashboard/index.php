               <!-- Begin Page Content -->
               <div class="container-fluid">
               	<?php if($students->status == 'diproses') :?>

               	<!-- Page Heading -->
               	<div class="d-sm-flex align-items-center justify-content-between mb-4">
               		<h1 class="h3 mb-0 text-gray-800">PPDB ONLINE</h1>
               	</div>

               	<!-- Content Row -->
               	<div class="row">
               		<div class="col-md-12">
               			<?php if($bukti_tf == NULL) :?>
               			<div class="alert alert-info" role="alert">
               				<h4 class="alert-heading">PENDAFTARAN BERHASIL</h4>
               				<p>Silahkan transfer ke nomor rekening 092029739 BCA, Atas nama SDI-ASSAADAH</p>
               			</div>
               			<?php else :?>
               			<div class="alert alert-info" role="alert">
               				<h4 class="alert-heading">UPLOAD BERHASIL</h4>
               				<p>SILAHKAN TUNGGU PENDAFTARAN ANDA SEDANG DIPROSES OLEH ADMIN</p>
               			</div>
               			<?php endif ;?>

               			<div class="card shadow mb-">
               				<div class="card-header py-3">
               					<h6 class="m-0 font-weight-bold text-primary">KONFIRMASI PEMBAYARAN</h6>
               				</div>

               				<div class="card-body">
               					<form action="<?php echo base_url(). 'wali/DashboardController/uploadTF'; ?>"
               						method="POST" enctype="multipart/form-data">

               						<div class="form-group col-md-12 was-validated">
               							<label for="document">BUKTI TRANSFER</label>
               							<div class="input-group mb-3">
               								<div class="custom-file">
               									<input type="hidden" name="id" value="<?= $stud_id ?>" id="">
               									<input type="file" class="custom-file-input" name="bukti_tf"
               										id="validatedCustomFile" required>
               									<label class="custom-file-label" for="validatedCustomFile">Choose
               										file...</label>
               									<div class="invalid-feedback">Example invalid custom file feedback
               									</div>
               								</div>
               							</div>
               							<?php if($bukti_tf == NULL) :?>
               							<p class="text-sm-left text-danger">silahkan upload bukti transfer!</p>
               							<?php else :?>
               							<p class="text-sm-left text-success">anda sudah mengupload bukti transfer</p>
               							<?php endif ;?>
               						</div>

               						<div class="modal-footer">
               							<button type="submit" class="btn btn-primary">UNGGAH</button>
               						</div>

               					</form>
               				</div>
               			</div>
               		</div>
               		<?php elseif($students->status == 'diterima') :?>
               		<!-- Page Heading -->
               		<div class="d-sm-flex align-items-center justify-content-between mb-4">
               			<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
               		</div>

               		<!-- Content Row -->
               		<div class="row">
               			<div class="col-md-12">
               				<!-- Earnings (Monthly) Card Example -->
               				<div class="col-xl-12 col-md-6 mb-4">
               					<div class="card border-left-primary shadow h-100 py-2">
               						<div class="card-body">
               							<div class="row no-gutters align-items-center">
               								<div class="col mr-2">
               									<?php if($getAbsen == NULL) :?>
               									<div
               										class="text-xs font-weight-bold text-primary text-uppercase mb-1">
               										Status Absen Hari InI</div>
               									<div class="h5 mb-0 font-weight-bold text-gray-800">Tidak ada absen
               										hari ini</div>
               									<?php else :?>
               									<?php foreach($getAbsen as $absen) :?>
               									<div
               										class="text-xs font-weight-bold text-primary text-uppercase mb-1">
               										Status Absen Hari InI</div>
               									<div class="h5 mb-0 font-weight-bold text-gray-800">
               										<?php echo $absen->status ?></div>
               									<?php endforeach ;?>
               									<?php endif ;?>
               								</div>
               								<div class="col-auto">
               									<i class="fas fa-calendar fa-2x text-gray-300"></i>
               								</div>
               							</div>
               						</div>
               					</div>
               				</div>

               			</div>
               		</div>

               		<?php else :?>
               		<!-- Page Heading -->
               		<div class="d-sm-flex align-items-center justify-content-between mb-4">
               			<h1 class="h3 mb-0 text-gray-800">PPDB ONLINE</h1>
               		</div>

               		<!-- Content Row -->
               		<div class="row">
               			<div class="col-md-12">
               				<div class="card shadow mb-">
               					<div class="card-body">
               						<form action="<?php echo base_url(). 'wali/DashboardController/store'; ?>"
               							method="POST" enctype="multipart/form-data">
               							<h6 class="m-0 font-weight-bold mb-2">KETERANGAN MURID</h6>
               							<div class="form-group col-md-12">
               								<label for="username">NAMA LENGKAP</label>
               								<input type="hidden" class="form-control" name="user_id"
               									value="<?= $id ?>">
               								<input type="text" class="form-control" name="nama_lengkap" required>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="lastname">NAMA PANGGILAN</label>
               								<input type="text" class="form-control" name="nama_p" required>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="gender">JENIS KELAMIN</label>
               								<select name="gender" id="gender" class="form-control">
               									<option value="">PILIH JENIS KELAMIN</option>
               									<option value="Laki-laki">Laki-laki</option>
               									<option value="Perempuan">Perempuan</option>
               								</select>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="lastname">TEMPAT LAHIR</label>
               								<input type="text" class="form-control" name="tempat" required>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="lastname">TANGGAL LAHIR</label>
               								<input type="date" class="form-control" name="tgl_lahir" required>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="lastname">UMUR</label>
               								<input type="text" class="form-control" name="umur" required>
               							</div>
               							<br>

               							<h6 class="m-0 font-weight-bold mb-2">KETERANGAN ORANG TUA MURID</h6>

               							<div class="form-group col-md-12">
               								<label for="lastname">NAMA AYAH</label>
               								<input type="text" class="form-control" name="nama_a" required>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="lastname">NAMA IBU</label>
               								<input type="text" class="form-control" name="nama_i" required>
               							</div>
               							<br>
               							<h6 class="m-0 font-weight-bold mb-2">PEKERJAAN</h6>

               							<div class="form-group col-md-12">
               								<label for="lastname">AYAH</label>
               								<input type="text" class="form-control" name="pekerjaan_a" required>
               							</div>

               							<div class="form-group col-md-12">
               								<label for="lastname">IBU</label>
               								<input type="text" class="form-control" name="pekerjaan_i" required>
               							</div>
               							<br>
               							<h6 class="m-0 font-weight-bold mb-2">PENGHASILAN</h6>

               							<div class="form-check form-group" require>
               								<input class="form-check-input" type="checkbox" name="penghasilan"
               									id="status_masuk" value="<1.000.000">
               								<label class="form-check-label" for="defaultCheck1">
               									< Rp 1.000.000 </label> </div> <div class="form-check form-group">
               										<input class="form-check-input" type="checkbox" name="penghasilan" id="status_masuk"
               											value=">1.000.000">
               										<label class="form-check-label" for="defaultCheck2">
               											> Rp 1.000.000
               										</label>
               							</div>

               							<div class="modal-footer">
               								<button type="submit" class="btn btn-primary">SIMPAN</button>
               							</div>

               						</form>
               					</div>

               				</div>
               			</div>
               			<?php endif ;?>
               		</div>
               		<!-- /.container-fluid -->

               	</div>
               	<!-- End of Main Content -->
