                <!-- Begin Page Content -->
                <div class="container-fluid">

                	<!-- Page Heading -->
                	<div class="d-sm-flex align-items-center justify-content-between mb-4">
                		<h1 class="h3 mb-0 text-gray-800">DATA NILAI ANAK</h1>
                	</div>

                	<!-- Content Row -->
                	<div class="row">
                		<div class="col-md-12">
                			<div class="card shadow mb-">
                				<!-- <div class="card-header py-3">
                					<h6 class="m-0 font-weight-bold text-primary">Detail Absent</h6>
                				</div> -->

                				<div class="card-body">
                					<form class="form-inline" action="<?php echo base_url('wali/ChildController/byDatePoint/'. $stud_id ) ?>" method="POST">
                						<div class="form-group mx-sm-3 mb-2">
                							<input type="date" class="form-control" id="inputPassword2"
                								placeholder="" name="date">
                						</div>
                						<button type="submit" class="btn btn-info mb-2"><i class="fa fa-search"></i></button>
                					</form>
                					<div class="table-responsive">
                						<table class=" table table-bordered table-hover" id="table-id"
                							style="font-size:13px;">
                							<thead>
                								<th>MATA PELAJARAN</th>
                								<th>NILAI</th>
												<th>NILAI MINIMAL</th>
												<th>STATUS</th>
												<th>TANGGAL</th>
                							</thead>
                							<tbody>
												<?php if($getPoint == NULL) :?>
													<div class="alert alert-info d-flex align-items-center" role="alert">
                									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                										fill="currentColor"
                										class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2"
                										viewBox="0 0 16 16" role="img" aria-label="Warning:">
                										<path
                											d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                									</svg>
                									<div>
                										DATA NILAI KOSONG
                									</div>
                								</div>
												<?php else :?>
                								<?php foreach($getPoint as $point) :?>
                								<tr>
                									<td><?php echo $point->name ?></td>
                									<td><?php echo $point->point ?></td>
													<td><?php echo $point->point_kkm ?></td>
													<?php if($point->point < $point->point_kkm ) :?>
													<td><button class="btn btn-warning badge">NILAI KURANG</button></td>
													<?php else :?>
													<td><button class="btn btn-success badge">NILAI SEMPURNA</button></td>
													<?php endif ;?>
													<td><?php echo $point->date_point ?></td>
                								</tr>
                								<?php endforeach ;?>
												<?php endif ;?>
                							</tbody>
                						</table>
                					</div>
                				</div>
                			</div>
                		</div>

                	</div>
                	<!-- End of Main Content -->
                </div>
